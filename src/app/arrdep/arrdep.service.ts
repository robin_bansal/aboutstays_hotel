import { Injectable } from '@angular/core';
import { Http, Request, RequestMethod, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map' ;

import * as globals from '../globals';


@Injectable()
export class ArrdepService {

	constructor( private http: Http) {}

	url :String = "http://" + globals.apiBaseurl + "/aboutstays";
	bookingDate = "05-07-2017";

	getUnallocatedBookings( data : String ){
		const api = `${this.url}/arrivalsAndDepartures/getUnCompleteBookings?hotelId=${data}&bookingDate=05-07-2017`
		return this.http.get(api).map( 
			response => response.json() 
		);
	}
	getWingsAll(){
	 	const api = `${this.url}/wings/getAll`;
		return this.http.get(api).map( 
			response => response.json() 
		);
	}
	getWingsByHotel( data : String ){
		const api = `${this.url}/wings/getByHotelId?hotelId=${data}`;
		return this.http.get(api).map(
			response => response.json()
		)
	}

	getFloorsAll(){
		const api = `${this.url}/floors/getAll`;
		return this.http.get(api).map( 
			response => response.json() 
		);
	}

	getFloorsByWingl( data : String ){
		const api = `${this.url}/floors/getByWingId?wingId=${data}`;
		return this.http.get(api).map( 
			response => response.json() 
		);
	}

	getRoomsAndStays( floor : String , date : String){
		const api = `$this.url/arrivalAndDepartures/getRoomAndStays`
		return this.http.post(api,JSON.stringify( { floorId:floor,date:date })).map( 
			response => response.json() 
		);
	}

	getRoomCategoryAll(){
		const api = `${this.url}/roomCategory/getAll`;
		return this.http.get(api).map( 
			response => response.json() 
		);
	}

	getRoomCategoryByHotelId( data : String ){
		const api = `${this.url}/roomCategory/getByHotelId?hotelId=${data}`;
		return this.http.get(api).map( 
			response => response.json() 
		);
	}

	getRoomsByFLoor( data : String ){
		const api = `${this.url}/room/getByFloorId?floorId=${data}`;
		return this.http.get(api).map( 
			response => response.json() 
		);

	}

}