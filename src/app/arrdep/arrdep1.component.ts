import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import { ArrdepService } from './arrdep.service';
import { ResponseUnAllocatedRooms } from './response';

@Component({
  selector: 'my-app1',
  templateUrl: './arrdep1.html',
  styleUrls: ["css/arrdep.css"],
  providers : [ArrdepService]
})




export class Arrdep1Component implements OnInit{
	cards : ResponseUnAllocatedRooms[];
	hotelId : String = "59143728e4b08d3751f8c49f";

	constructor( private service:ArrdepService){}

	ngOnInit(){
		this.service.getUnallocatedBookings( this.hotelId ).subscribe( 
			response => {this.cards = response.data,
			console.log(this.cards),
			console.log(response)
			}
		);
	}

}
