import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {CommonModule} from '@angular/common';

import { AppRoutingModule } from './arrdep.routes';
import { ArrdepComponent } from './arrdep.component';
import { Arrdep1Component } from './arrdep1.component';

import { Arrdep2Component } from './arrdep2.component';


@NgModule({
	imports: [AppRoutingModule, HttpModule,CommonModule],
	declarations: [
				ArrdepComponent,
				Arrdep1Component,
				Arrdep2Component
			],
	bootstrap: [ArrdepComponent]
})

export class ArrdepModule{  }