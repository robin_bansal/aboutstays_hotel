import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap , Params}   from '@angular/router';
import { Location }                 from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { ArrdepService } from './arrdep.service';
import { ResponseRoomCategory } from './response';


@Component({
  selector: 'my-app2',
  templateUrl: './arrdep2.html',
  styleUrls: ["css/arrdep.css"],
   providers : [ArrdepService]
})

export class Arrdep2Component implements OnInit{
	roomCategories : ResponseRoomCategory[];
	reservationId : String ;
	hotelId : String = "59143728e4b08d3751f8c49f";

	constructor( private service:ArrdepService, private route:ActivatedRoute, private location:Location ){}

	ngOnInit(){
		this.route.params.subscribe((params: Params) => {
			this.reservationId = params['id'],
			console.log(this.reservationId)
		} );
		this.service.getRoomCategoryByHotelId(this.hotelId).subscribe( 
			response => { this.roomCategories = response.data,
			console.log(this.roomCategories[0].earlyCheckinInfo ),
			console.log(response.data)
			}
		);
		
	}

	

}