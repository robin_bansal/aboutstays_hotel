
import { Routes,RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


import { Arrdep1Component } from './arrdep1.component'; 
import { Arrdep2Component } from './arrdep2.component'; 

const routes : Routes= [
	
   	{ path : '', component : Arrdep1Component , pathMatch : 'full' },
    { path : 'allocate/:id', component : Arrdep2Component }
];


@NgModule({

	imports : [
		RouterModule.forChild(routes)
		],

	exports : [RouterModule]
})

export class AppRoutingModule {}

