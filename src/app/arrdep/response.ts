export class ResponseUnAllocatedRooms{
	"checkInDate": "";
	"checkOutDate": "";
	"bookedByUser" : {	
		"name" : "";
		"date" : "";
	};
	"bookedForUser"	: {
		"name" : "";
		"date" : "";
		"pax" : "";
	};	
	"reservationNumber" : "";
	"bookingId" : "";
	"roomCategoryResponse" : "";	
	"getRoomResponse" : "";	
	"addedToStayline" : "";
	"getStayResponse" : {
		"staysId" :	"";
		"bookingId"	: "";
		"hotelId" : "";
		"roomId" : 	"";
		"userId" : 	"",
		"checkInDate" : "";
		"checkInTime" : "";
		"checkOutTime" : "";
		"checkOutDate" : "";
		"official" : "";
		"staysOrdering" : "";
	};	
	
}

export class ResponseRoomCategory{
	"categoryName" : "";
	"categoryCode" : "";
	"count" : "";
	"imageUrl" : "";
	"description" : "";
	"size" : "";
	"sizeMetric" : "";
	"adultCapacity" : "";
	"childCapacity" : "";
	"additionalImageUrls" : "";
	"earlyCheckinInfo" : {
		"standardCheckinTime" : "";
		"listDeviationInfo" : "";
	};
	"lastCheckOutInfo" : {
		"standardCheckoutTime" : "";
		"listDeviationInfo" : "" ;
	};
	"hotelId" : "";
	"roomCategoryId" : "";
	"inRoomAmenities" : "";
	"preferenceBasedAmenities" : "";

}