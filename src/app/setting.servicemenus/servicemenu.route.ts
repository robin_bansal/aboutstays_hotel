import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingServicemenuComponent } from './setting.servicemenu.component';


export const routes: Routes = [
  { path: '',  component: SettingServicemenuComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);