import { Component, OnInit } from '@angular/core';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import { RoomServiceModel } from '../models/roomservice.model';
import { HouseKeepingServiceModel } from '../models/housekeepingservice.model';
import { LaundryServiceModel } from '../models/laundryservice.model';
import { InternetServiceModel } from '../models/internetservice.model';
import { ComplementryServiceModel } from '../models/complementry.model';
import { EntertainmentServiceModel } from '../models/entertainment.model';
import { RoomCategory } from '../models/floormap.model';
import { FloormapService } from '../services/floormap.services';
import { CommuteServiceModel } from '../models/commuteservices.model';
import { MasterChannelsModel } from '../models/masterchannels.model';

import { ServiceMenuService } from '../services/servicemenu.service';
@Component({
  selector: 'my-app',
  templateUrl: './setting.servicemenu.html',
  styleUrls: ["css/setting.css"],
  providers:[FloormapService]
})

export class SettingServicemenuComponent extends ServiceMenuService {
	public accord: Array<any>;
	public roomservice = new RoomServiceModel;
	public roomsersviceApiStatus=false;
	public roomsersviceCatApiStatus: Array<any>;

	public housekeeping = new HouseKeepingServiceModel;
	public housekeepingApiStatus=false;
	public housekeepingCatApiStatus: Array<any>;

	public laundryservice = new LaundryServiceModel;
	public laundryserviceApiStatus=false;
	public laundryserviceCatApiStatus: Array<any>;

	public internetservice = new InternetServiceModel;
	public internetserviceApiStatus=false;
	public internetserviceCatApiStatus: Array<any>;

	public complementryservice = new ComplementryServiceModel;
	public complementryserviceApiStatus=false;

	public entertainmentservice = new EntertainmentServiceModel;
	public entertainmentserviceApiStatus=false;
	public editEntertainmentData: any;
	
	public roomcategory=new RoomCategory;
	public roomcatApiStatus=false;

	public commuteservices=new CommuteServiceModel;
	public commuteservicesApiStatus=false;
	public commuteEditData:any;

	public masterchannels = new MasterChannelsModel;
	public masterChannelsApiStatus=false;

	public slaEditData={
			"slaApplicable": false,
            "slaTime": 0
		};
	public serviceTimeEditData={
			"startTime": false,
            "endTime": 0
		};
	public roomserviceEditStatus: Array<boolean>;
	public roomserviceItemEditData = new RoomServiceModel;
	public itemEditData: any;

	public itemEditStatus: Array<any>;


	public roomCategoryEdit: any;

	public roomserviceTimeEditData: any;
	
	constructor(_http: Http, private _floormapservice: FloormapService)
	{
		super(_http);
		this.accord=Array();
		this.roomsersviceCatApiStatus=Array();

		this.housekeepingCatApiStatus=Array();

		this.laundryserviceCatApiStatus=Array();

		this.internetserviceCatApiStatus=Array();
		this.roomserviceEditStatus=[];

		this.itemEditStatus=Array();
	}

	ngOnInit(): void{

		//check if room srvice accordance open then call api
		//console.log(this.roomservice.model.listMenuItem);
	}

	//room service start here
	roomserviceExpand(accord_status: any)
	{
		if(accord_status==true)
		{
			this.roomsersviceApiStatus=false;
		
			this.getRoomSrvices()
				.subscribe(
	            res => {this.roomservice.service=res.data; this.roomsersviceApiStatus=true;},
	            error => console.log(error),
	            () => {}
	            );
		}
	}
	roomServiceTimeEdit(service: any)
	{
		this.roomserviceTimeEditData=JSON.parse(JSON.stringify(service));
	}
	roomserviceSubmit(service: any)
	{
		service.startTime=this.roomserviceTimeEditData.startTime;
		service.endTime=this.roomserviceTimeEditData.endTime;
		service.allDay=this.roomserviceTimeEditData.allDay;
	}
	deleteRoomServiceItem(type:any, itemId: any)
	{
		var conf=confirm("Are your sure to delete this item?");
		if(conf)
		{
			this.roomservice.deleteItem(type, itemId);
			return true;
		}
		return false;
	}

	roomserviceItemExpand(item_type: any, index: any)
	{
		if(this.accord[index]==true)
		{
			this.roomsersviceCatApiStatus[index]=false;
			this.getRoomSrvicesFood(this.roomservice.service.id, item_type)
				.subscribe(
	            res => {(res.error==false)?this.roomservice.items[item_type]=res.data:'error';this.roomsersviceCatApiStatus[index]=true;},
	            error => console.log(error),
	            () => {}
	            );
	    }
	}
	roomServiceItemEditInit(type:any, itemId: any)
	{
		let data=this.roomservice.getItemByTypeAndId(type, itemId);
		this.roomserviceItemEditData=JSON.parse(JSON.stringify(data));
	}
	addRoomServiceItemInit(service:any, category: any)
	{
		var temp = new RoomServiceModel;
		this.roomserviceItemEditData = temp.items[0];
		this.roomserviceItemEditData['type']=service.type;
		this.roomserviceItemEditData['hotelId']=this.loginDetails.hotelId;
		this.roomserviceItemEditData['roomServicesId']=this.roomservice.service.id;
		this.roomserviceItemEditData['category']=category;


	}
	submitRoomServiceItem(isvalid: boolean)
	{
		if(!isvalid)
			return false;
		
		if(this.roomserviceItemEditData['fooditemId']=="")
		{
			var data={
			    "roomServicesId": this.roomserviceItemEditData['roomServicesId'],
			    "hotelId": this.roomserviceItemEditData['hotelId'],
			    "imageUrl": this.roomserviceItemEditData['imageUrl'],
			    "name": this.roomserviceItemEditData['name'],
			    "serves": this.roomserviceItemEditData['serves'],
			    "cuisineType": this.roomserviceItemEditData['cuisineType'],
			    "mrp": this.roomserviceItemEditData['mrp'],
			    "price": this.roomserviceItemEditData['price'],
			    "description": this.roomserviceItemEditData['description'],
			    "foodLabel": this.roomserviceItemEditData['foodLabel'],
			    "spicyType": this.roomserviceItemEditData['spicyType'],
			    "recommended": this.roomserviceItemEditData['recommended'],
			  	"type" : this.roomserviceItemEditData['type']
			    
			  }
			  this.startLoader();
			this.addFoodItem(data)
					.subscribe(
			            res => {console.log(res); this.roomservice.addItem(this.roomserviceItemEditData);this.stopLoader()},
			            error => console.log(error),
			            () => {}
		            );
		}else
		{
			this.roomservice.updateItem(this.roomserviceItemEditData);
		}
	    return true;
		
	}
	
	
	slaEdit(obj: any)
	{
		this.slaEditData.slaApplicable=obj.service.slaApplicable;
		this.slaEditData.slaTime=obj.service.slaTime;
		return true;
	}
	slaSubmit(obj: any)
	{
		obj.service.slaApplicable=this.slaEditData.slaApplicable;
		obj.service.slaTime=this.slaEditData.slaTime;	
		return true;
	}
	serviceTimeEdit(obj: any)
	{
		this.serviceTimeEditData.startTime=obj.service.startTime;
		this.serviceTimeEditData.endTime=obj.service.endTime;
		return true;
	}
	submitserviceTime(obj: any)
	{
		obj.service.startTime=this.serviceTimeEditData.startTime;
		obj.service.endTime=this.serviceTimeEditData.endTime;	
		return true;
	}
	


	//housekeeping start here

	houseKeepingExpand(accord_status: any)
	{
		if(accord_status==true)
		{
			this.housekeepingApiStatus=false;
		
			this.getHouseKeepingSrvices()
				.subscribe(
	            res => {this.housekeeping.service=res.data; this.housekeepingApiStatus=true;},
	            error => console.log(error),
	            () => {}
	            );
		}
	}
	

	houseKeepingItemExpend(item_type: any, index: any)
	{
		if(this.accord[index]==true)
		{
			this.housekeepingCatApiStatus[index]=false;
			this.getHouseKeepingItem(this.housekeeping.service.id, item_type)
				.subscribe(
	            res => {(res.error==false)?this.housekeeping.items[item_type]=res.data:'error';this.housekeepingCatApiStatus[index]=true;},
	            error => console.log(error),
	            () => {}
	            );
	    }
	}

	houseKeepingItemEditInit(type: any, category: any)
	{
		var temp=this.housekeeping.getCategoryItems(type, category);
		this.itemEditData=Array();
		temp.forEach(function(value:any){
			var obj=JSON.parse(JSON.stringify(value));
			this.itemEditData.push(obj);	
		}, this)
		
	}
	submitHousekeepingAllSubmit()
	{
		this.housekeeping.updateAllItem(this.itemEditData);
	}

	addHousekeepingItem(service:any, category: any)
	{
		var temp = new HouseKeepingServiceModel;
		var data = temp.items[0];
		data['type']=service.type;
		data['hotelId']=this.loginDetails.hotelId;
		data['housekeepingServicesId']=this.housekeeping.service.id;
		data['category']=category;

		this.housekeeping.addItem(data);

		this.houseKeepingItemEditInit(service.type, category);

		return true;

	}
	
	submitHousekeepingItem(isvalid: boolean)
	{
		if(!isvalid)
			return false;
		

		var data={
		    "housekeepingServicesId": this.itemEditData['housekeepingServicesId'],
		    "hotelId": this.itemEditData['hotelId'],
		    "name": this.itemEditData['name'],
		    "type": this.itemEditData['type'],
		    "mrp": this.itemEditData['mrp'],
		    "price": this.itemEditData['price'],
		    "category": this.itemEditData['category'],
		    
		  }
		  this.startLoader();
		this.addHouseKeepingItem(data)
				.subscribe(
		            res => {this.housekeeping.addItem(this.itemEditData);this.stopLoader()},
		            error => {this.throwError(error);},
		            () => {}
	            );
	    return true;
		
	}

	deleteHousekeepingItem(type:any, itemId: any)
	{
		var conf=confirm("Are your sure to delete this item?");
		if(conf)
		{
			this.startLoader();
			this._deleteHouseKeepingItem(itemId)
					.subscribe(
			            res => {this.housekeeping.deleteItem(type, itemId);this.stopLoader()},
			            error => {this.throwError(error)},
			            () => {}
		            );
			return true;
		}
		return false;
	}


	//laundry service start here
	
	

	laundryServiceExpand(accord_status: any)
	{
		if(accord_status==true)
		{
			this.laundryserviceApiStatus=false;
		
			this.getLaundryService()
				.subscribe(
	            res => {this.laundryservice.service=res.data; this.laundryserviceApiStatus=true;},
	            error => console.log(error),
	            () => {}
	            );
		}
	}

	laundryServiceItemExpend(item_type: any, index: any)
	{
		if(this.accord[index]==true)
		{
			this.laundryserviceCatApiStatus[index]=false;
			this.getLaundryServiceItem(this.laundryservice.service.serviceId, item_type)
				.subscribe(
	            res => {(res.error==false)?this.laundryservice.items[item_type]=res.data:'error';this.laundryserviceCatApiStatus[index]=true;},
	            error => console.log(error),
	            () => {}
	            );
	    }
	}

	laundryItemEditInit(type: any, category: any)
	{
		var temp=this.laundryservice.getCategoryItems(type, category);
		this.itemEditData=Array();
		temp.forEach(function(value:any){
			var obj=JSON.parse(JSON.stringify(value));
			this.itemEditData.push(obj);	
		}, this)
		
	}
	submitLaundryAllSubmit()
	{
		this.laundryservice.updateAllItem(this.itemEditData);
	}

	addLaundryItem(service:any, category: any)
	{
		var temp = new LaundryServiceModel;
		var data = temp.items[0];
		data['type']=service.type;
		data['hotelId']=this.loginDetails.hotelId;
		data['laundryServicesId']=this.laundryservice.service.serviceId;
		data['category']=category;

		this.laundryservice.addItem(data);

		this.laundryItemEditInit(service.type, category);

		return true;

	}

	deleteLaundryItem(type:any, itemId: any)
	{
		var conf=confirm("Are your sure to delete this item?");
		if(conf)
		{
			this.startLoader();
			this._deleteLaundryItem(itemId)
					.subscribe(
			            res => {this.laundryservice.deleteItem(type, itemId);this.stopLoader()},
			            error => {this.throwError(error)},
			            () => {}
		            );
			return true;
		}
		return false;
	}

	//internet service start here

	internetServiceExpand(accord_status: any)
	{
		if(accord_status==true)
		{
			this.internetserviceApiStatus=false;
		
			this.getInternetService()
				.subscribe(
	            res => {this.internetservice.service=res.data; this.internetserviceApiStatus=true;},
	            error => console.log(error),
	            () => {}
	            );
		}
	}

	internetserviceEditInit(category: any)
	{
		var temp=this.internetservice.getCategoryItems(category);
		this.itemEditData=Array();
		temp.forEach(function(value:any){
			var obj=JSON.parse(JSON.stringify(value));
			this.itemEditData.push(obj);	
		}, this)
		
	}
	submitInternetServiceAllItem()
	{
		this.internetservice.updateAllItem(this.itemEditData);
	}
	deleteInternetItem(itemObj:any)
	{
		var conf=confirm("Are your sure to delete this item?");
		if(conf)
		{
			this.internetservice.deleteItem(itemObj);
			return true;
		}
		return false;
	}

	addInternetServiceItem(service: any)
	{
		console.log(service);
		var temp = new InternetServiceModel;
		var data = temp.service.listPacks[0];
		data['category']=service.category;
		data['showOnApp']=false;

		this.internetservice.addItem(data);

		this.internetserviceEditInit(service.category);

		return true;

	}

	//complementry service start here

	complementryServiceExpand(accord_status: any)
	{
		if(accord_status==true)
		{
			this.complementryserviceApiStatus=false;
		
			this.getComplementryService()
				.subscribe(
	            res => {this.complementryservice.loadModel(res.data); this.complementryserviceApiStatus=true;},
	            error => console.log(error),
	            () => {}
	            );
		}
	}

	complementryItemEditInit(tabName: string, title: string)
	{
		let items=this.complementryservice.findItemByTabAndTitle(tabName, title);
		this.itemEditData=Array();
		items.descriptionList.forEach(function(value: any){
			this.itemEditData.push({"description": JSON.parse(JSON.stringify(value))});	
		}, this)
		console.log(this.itemEditData);
	}

	submitcomplementryAllSubmit(tabName: string, title: string)
	{
		let data=this.complementryservice.findItemByTabAndTitle(tabName, title);
		let descList=Array();
		this.itemEditData.forEach(function(value: any){
			descList.push(value.description);
		})
		data.descriptionList=descList;
	}

	deleteComplementryItem(tabName: string, title: string, index: number)
	{
		let conf=confirm("Are you sure to delete this item?");
		if(conf)
			this.complementryservice.deleteItem(tabName, title, index);
	}

	addComplementryItem(tab: string, title: string)
	{
		
		var data = "";
		

		this.complementryservice.addItem(tab, title, data);

		this.complementryItemEditInit(tab, title);

		return true;

	}

	//enternainment service start here

	entertainmentServiceExpand(accord_status: any)
	{
		if(accord_status==true)
		{
			this.entertainmentserviceApiStatus=false;
		
			this.getEntertainmentService()
				.subscribe(
	            res => {this.entertainmentservice.loadModel(res.data); this.entertainmentserviceApiStatus=true;},
	            error => console.log(error),
	            () => {}
	            );
		}
	}

	//early check in start here

	earlyCheckInOpen(accord_status: boolean)
	{
		if (accord_status == true && this.roomcatApiStatus==false) {
			this.roomcatApiStatus=false;
		
			this._floormapservice.loadRoomCategories()
				.subscribe(
	            res => {this.roomcategory.loadModel(res.data); this.roomcatApiStatus=true;},
	            error => this.throwError(error),
	            () => {}
	            );	
		}
	}
	earlyCheckInEditInit()
	{
		let temp=this.roomcategory.model;
		
		this.roomCategoryEdit=JSON.parse(JSON.stringify(temp));
		this.roomCategoryEdit.forEach(function(value: any){
			if(value.earlyCheckinInfo.listDeviationInfo==null)
				value.earlyCheckinInfo.listDeviationInfo=[];
			
			let temp1=Array();
			value.earlyCheckinInfo.listDeviationInfo.forEach(function(value1: any){
				temp1.push(value1.deviationInHours);
			});

			for(let i=1; i<=7; i++)
			{
				if(temp1.indexOf(i)<0)
				{
					value.earlyCheckinInfo.listDeviationInfo.push({
						"deviationInHours": i,
                        "price": ""
					});		
				}
			}
		});
		

	}

	getEarlyCheckInPrice(obj: any, hour: any)
	{
		var returns="";
		if(obj.earlyCheckinInfo.listDeviationInfo!=null && obj.earlyCheckinInfo!=null)
		{
			obj.earlyCheckinInfo.listDeviationInfo.forEach(function(value: any){
					if(value.deviationInHours>=hour && value.deviationInHours<hour+1)
					{
						returns = value.price;
					}
				
			});
		}

		return returns;
		
	}
	earlyCheckInSubmit(valid: boolean)
	{
		if(valid)
		{
			this.roomcategory.model = this.roomCategoryEdit;
			return true;
		}
		return false;
	}

	//late checkout
	earlyCheckOutEditInit()
	{
		let temp=this.roomcategory.model;
		
		this.roomCategoryEdit=JSON.parse(JSON.stringify(temp));
		this.roomCategoryEdit.forEach(function(value: any){
			if(value.lateCheckoutInfo.listDeviationInfo==null)
				value.lateCheckoutInfo.listDeviationInfo=[];
			
			let temp1=Array();
			value.lateCheckoutInfo.listDeviationInfo.forEach(function(value1: any){
				temp1.push(value1.deviationInHours);
			});

			for(let i=1; i<=7; i++)
			{
				if(temp1.indexOf(i)<0)
				{
					value.lateCheckoutInfo.listDeviationInfo.push({
						"deviationInHours": i,
                        "price": ""
					});		
				}
			}
		});
		

	}
	
	getLateCheckOutPrice(obj:any, hour: any)
	{
		var returns="";
		if(obj.lateCheckoutInfo.listDeviationInfo!=null && obj.lateCheckoutInfo!=null)
		{
			obj.lateCheckoutInfo.listDeviationInfo.forEach(function(value:any){
					if(value.deviationInHours==hour)
					{
						returns = value.price;
					}
				
			});
		}

		return returns;
		
	}

	//commute start here
	CommuteServiceOpen(accord_status: boolean)
	{
		if (accord_status == true) {
			this.commuteservicesApiStatus=false;
			this.loadCommuteServices()
				.subscribe(
	            res => {this.commuteservices.loadModel(res.data.listCommutePackage); this.commuteservicesApiStatus=true;},
	            error => this.throwError(error),
	            () => {}
	            );
		}
	}
	commuteEditInit(service: any)
	{
		this.commuteEditData=JSON.parse(JSON.stringify(service));
		console.log(this.commuteEditData);
	}
	submitCommute(index: number)
	{
		this.commuteservices.model[index]=this.commuteEditData;
		return true;
	}

	//channels
	loadMasterChannelsData()
	{
		if (this.masterChannelsApiStatus == false) {
			this.startLoader();
			this.loadMasterChannels()
				.subscribe(
	            res => {
	            	if(!res.error)
	            	{
	            		this.masterchannels.loadModel(res.data.channels); this.masterChannelsApiStatus=true;
	            		this.stopLoader();
	            	}else
	            	{
	            		this.throwError("Somthing wrong");
	            	}
	            },
	            error => this.throwError(error),
	            () => {}
	            );
		}
	}

	addEntertainmentInit()
	{

		this.loadMasterChannelsData();
		this.editEntertainmentData=Array();
		this.initNewChannel();
		
	}
	submitAddEntertainment(formvalue: any, valid: boolean)
	{
		if(!valid)
		{
			return false;
		}
		//console.log(this.editEntertainmentData);
		this.editEntertainmentData.forEach(function(value: any){
			value.itemDetails=this.masterchannels.findById(value.itemDetails.channelId);
			this.entertainmentservice.save(value);	
		}, this)
		
		//console.log(newEntertainment);
		return true;
	}
	editEntertainmentInit()
	{
		this.editEntertainmentData=JSON.parse(JSON.stringify(this.entertainmentservice.model));
	}
	submitEntertainment()
	{
		this.entertainmentservice.loadModel(this.editEntertainmentData);
	}
	initNewChannel()
	{
		let temp=new EntertainmentServiceModel;
		let newEntertainment=temp.model[0];
		newEntertainment.hotelId=this.loginDetails.hotelId;
		this.editEntertainmentData.push(newEntertainment);
	}
	deleteEntertainment(channel: any)
	{
		let conf=confirm("Are you sure to delete this item?");
		if(conf)
		{
			this.entertainmentservice.deleteItem(channel.itemId);
			let temp=Array();
			this.editEntertainmentData.forEach(function(value: any){
				if(value.itemId!=channel.itemId)
				{
					temp.push(value);
				}
			})
			this.editEntertainmentData=temp;
		}
	}


}