import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { routing } from './servicemenu.route';
import { FormsModule, NgForm, FormGroup, Validators, Validator }    from '@angular/forms';
import { FloormapService } from '../services/floormap.services';
import { SettingServicemenuComponent } from './setting.servicemenu.component';

@NgModule({
	imports: [routing,CommonModule,HttpModule,FormsModule],
	declarations: [SettingServicemenuComponent, SettingServicemenuComponent],
	bootstrap: [SettingServicemenuComponent]
})

export class ServiceMenuModule{  }