import { Injectable, Inject } from '@angular/core';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';

import * as globals from '../globals';

@Injectable()

export class CommonServices
{
  public masterAmeneties: Array<any>;
	constructor (public _http: Http) {}
	
    
  
  apiGet(url: any){
   let headers = new Headers;

   headers.append("Access-Control-Allow-Origin", "*");
   headers.append("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
   headers.append("Access-Control-Allow-Headers", "X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description");
   
   let options = new RequestOptions({ headers: headers });
   //var req_url="https://reqres.in/api/users?page=2";
   var req_url=url;
   //var req_url="http://192.168.1.11:8080/aboutstays/hotel/generalInfo?hotelId=59143728e4b08d3751f8c49f";

   return this._http.get(req_url)
                    .map(res => res.json());
  }

  apiDelete(url: any){
   let headers = new Headers;

   headers.append("Access-Control-Allow-Origin", "*");
   headers.append("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
   headers.append("Access-Control-Allow-Headers", "X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description");
   
   let options = new RequestOptions({ headers: headers });
   //var req_url="https://reqres.in/api/users?page=2";
   var req_url=url;
   //var req_url="http://192.168.1.11:8080/aboutstays/hotel/generalInfo?hotelId=59143728e4b08d3751f8c49f";

   return this._http.delete(req_url)
                    .map(res => res.json());
  }

  apiPost(url: any, data: any){
   let headers = new Headers;

   headers.append("Access-Control-Allow-Origin", "*");
   headers.append("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
   headers.append("Access-Control-Allow-Headers", "X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description");
   
   let options = new RequestOptions({ headers: headers });
   //var req_url="https://reqres.in/api/users?page=2";
   var req_url=url;
   //var req_url="http://192.168.1.11:8080/aboutstays/hotel/generalInfo?hotelId=59143728e4b08d3751f8c49f";

   return this._http.post(req_url, data)
                    .map(res => res.json());
  }

  apiPatch(url: any, data: any){
   let headers = new Headers;

   headers.append("Access-Control-Allow-Origin", "*");
   headers.append("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
   headers.append("Access-Control-Allow-Headers", "X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description");
   
   let options = new RequestOptions({ headers: headers });
   //var req_url="https://reqres.in/api/users?page=2";
   var req_url=url;
   //var req_url="http://192.168.1.11:8080/aboutstays/hotel/generalInfo?hotelId=59143728e4b08d3751f8c49f";

   return this._http.patch(req_url, data)
                    .map(res => res.json());
  }

  apiPostMulipart(url: any, data: FormData){
   let headers = new Headers;
   
   headers.append("content-type", "multipart/form-data; boundary=------------874d8d3ggh7x847");
   let options = new RequestOptions({ headers: headers });
   //var req_url="https://reqres.in/api/users?page=2";
   var req_url=url;
   //var req_url="http://192.168.1.11:8080/aboutstays/hotel/generalInfo?hotelId=59143728e4b08d3751f8c49f";
   console.log(data);
   return this._http.post(req_url, data, options)
                    .map(res => res.json());
  }

  startLoader()
  {
    document.getElementById("wating").style.display="block";
  }

  stopLoader()
  {
    document.getElementById("wating").style.display="none";
  }


  quickLinkScroll(elementid: any) {
    var target_elm=document.getElementById(elementid);
    var elm_pos=target_elm.offsetTop;

    
    var cosParameter = elm_pos-220;
    //alert(cosParameter);
    
    window.scrollBy(0, cosParameter);

    return true;
    
  }

  accordClick(status: boolean){
    return !status;
  }
  accordClass(status: boolean)
  {
     if(status==true && status!=null)
     {
       return "rotateUp";
     } 

     return "";
  }
  popupTrigger(status: boolean)
  {
    return !status;
  }
  throwError(error: any)
  {
    console.log(error);
    this.stopLoader();
  }

  showAlert(type: String, message: any)
  {
    console.log(message);
  }

  loadMasterAmenities(type: number)
  {
    if(type==0)
    {
       var req_url="http://" +globals.apiBaseurl+ "/aboutstays/masterAmenity/getAll";
     }else
     {
       var req_url="http://" +globals.apiBaseurl+ "/aboutstays/masterAmenity/getAll?type=" + type;
     }

     return this.apiGet(req_url) 
  }

  timeConversion(msTime: number)
  {
    var formatedDate = new Date(msTime);
    var day=formatedDate.getDay();
    var month=formatedDate.getMonth();
    var year=formatedDate.getFullYear();
    var hours=formatedDate.getHours()>12?formatedDate.getHours()-12:formatedDate.getHours();
    var minuts=formatedDate.getMinutes();
    var ampm=(formatedDate.getHours()>11?'PM':'AM');    
    var returnDate=day+'/'+month+'/'+year+' '+hours+':'+minuts+' '+ampm;
    return returnDate;     
  }
  getLoginDetails()
  {
    return JSON.parse(localStorage['loginDetails']);
  }


  //common api calls
  loadMasterChannels(){
   var req_url=globals.MASTER_CHANNELS_GET_ALL;
     return this.apiGet(req_url);
  }
} 