import {Injectable, Inject} from '@angular/core';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';

import * as globals from '../globals';
import { CommonServices } from './common.services';

@Injectable()


export class HotelInfoService extends CommonServices
{
	
	
	constructor (public _http: Http) {
    super(_http);
  }
  	
    
  
  getHotelInfoById(hid: String){
   
   //var req_url="https://reqres.in/api/users?page=2";
   var req_url= "http://" +globals.apiBaseurl+ "/aboutstays/hotel/get?hotelId=" + globals.hotelID;
   //var req_url="http://192.168.1.11:8080/aboutstays/hotel/generalInfo?hotelId=59143728e4b08d3751f8c49f";

   return this.apiGet(req_url)
  }

  

  getHouseRules()
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/houseRules/get/?id=" + globals.hotelID;

     return this.apiGet(req_url); 
  }

  updateHotelGeneralInfo(data: any)
  {
      var req_url="http://" +globals.apiBaseurl+ "/aboutstays/hotel/update";

     return this.apiPost(req_url, data);
  }

  updateHouseRule(data: any)
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/houseRules/update";

     return this.apiPost(req_url, data); 
  }

  uploadFile(data: FormData)
  {
    var req_url="http://" +globals.apiBaseurl+ "/aboutstays/file/image/upload";

     return this.apiPostMulipart(req_url, data); 
  }

  updateAmenity(data: any)
  {
    var req_url="http://" +globals.apiBaseurl+ "/aboutstays/hotel/linkAmenities";

     return this.apiPatch(req_url, data);  
  }

  updateEssential(data: any)
  {

    var req_url="http://" +globals.apiBaseurl+ "/aboutstays/hotel/linkEssentials";

     return this.apiPatch(req_url, data);  
  }
  updateCardAccepted(data: any)
  {
    var req_url="http://" +globals.apiBaseurl+ "/aboutstays/hotel/linkCardAccepted";

     return this.apiPatch(req_url, data);  
  }
  updateAward(data: any)
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/hotel/linkAward";
     return this.apiPatch(req_url, data);   
  }
}