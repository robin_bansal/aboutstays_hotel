import { Injectable, Inject } from '@angular/core';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { CommonServices } from '../services/common.services'
import * as globals from '../globals';

@Injectable()

export class FloormapService extends CommonServices
{
	loginDetails:any;
	constructor (_http: Http) {
		super(_http);
		this.loginDetails=JSON.parse(localStorage['loginDetails']);
	}

	loadRoomCategories(){
	   
	   var req_url="http://" +globals.apiBaseurl+ "/aboutstays/roomCategory/getAll?hotelId=" + this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}


  loadWings()
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/wings/getByHotelId?hotelId=" + this.loginDetails.hotelId;
     return this.apiGet(req_url);    
  }

  loadFloorsByWingId(wingid: any)
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/floors/getByWingId?wingId=" + wingid;
     return this.apiGet(req_url);    
  }

  loadFloors()
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/floors/getByHotelId?hotelId=" + this.loginDetails.hotelId;
     return this.apiGet(req_url);    
  }

  loadRoomsByFloorId(floorid: any)
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/room/getByFloorId?floorId=" + floorid;
     return this.apiGet(req_url);    
  }

  loadRooms()
  {
     var req_url="http://" +globals.apiBaseurl+ "/aboutstays/room/getByHotelId?hotelId=" + this.loginDetails.hotelId;
     return this.apiGet(req_url);    
  }
  deleteRooms(roomIds: string)
  {
    if(roomIds!="")
    {
      var req_url=globals.ROOM_DELETE_GET + "?hotelId=" + this.loginDetails.hotelId + "&roomIds=" + roomIds;
      return this.apiDelete(req_url);
    }
  }

  addWing(data: any)
  {
      var req_url="http://" +globals.apiBaseurl+ "/aboutstays/wings/add";

     return this.apiPost(req_url, data);
  }

  addFloor(data: any)
  {
      var req_url="http://" +globals.apiBaseurl+ "/aboutstays/floors/add";

     return this.apiPost(req_url, data);
  }

  addRoom(data: any)
  {
      var req_url="http://" +globals.apiBaseurl+ "/aboutstays/room/create";
     return this.apiPost(req_url, data);
  }

  addRoomCat(data: any)
  {
      var req_url="http://" +globals.apiBaseurl+ "/aboutstays/roomCategory/create";
     return this.apiPost(req_url, data);
  }
  updateRoomCat(data: any)
  {
      var req_url="http://" +globals.apiBaseurl+ "/aboutstays/roomCategory/update";
     return this.apiPatch(req_url, data);
  }

  updateRoomsBatch(data: any)
  {
      var req_url=globals.ROOMS_BATCH_UPDATE_PATCH;
     return this.apiPatch(req_url, data);
  }
  updateRoomsAmenities(data: any)
  {
      var req_url=globals.ROOMS_AMENITY_UPDATE_PATCH;
     return this.apiPatch(req_url, data);
  }
  	
}