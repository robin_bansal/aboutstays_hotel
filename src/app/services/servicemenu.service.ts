import { Injectable, Inject } from '@angular/core';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { CommonServices } from '../services/common.services'
import * as globals from '../globals';

@Injectable()

export class ServiceMenuService extends CommonServices
{
	loginDetails:any;
	constructor (public _http: Http) {
		super(_http);
		this.loginDetails=JSON.parse(localStorage['loginDetails']);
	}

	getRoomSrvices(){
	   
	   var req_url="http://" +globals.apiBaseurl+ "/aboutstays/roomService/get?id=" + this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}

	addFoodItem(data: any)
  	{
      	var req_url=globals.FOOD_ADD_ITEM_URL;
     	return this.apiPost(req_url, data);
  	}

	

	getRoomSrvicesFood(service_id: any, item_type: any)
	{  var req_url="http://" +globals.apiBaseurl+ "/aboutstays/foodItem/fetchByType?roomServicesId="  + service_id + "&type=" + item_type;
	   return this.apiGet(req_url);	
	}

	getHouseKeepingSrvices(){
	   
	   var req_url="http://" +globals.apiBaseurl+ "/aboutstays/housekeepingService/get?id=" + this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}
	addHouseKeepingItem(data: any)
  	{
      	var req_url=globals.HOUSEKEEPING_ADD_ITEM_URL;
     	return this.apiPost(req_url, data);
  	}
  	_deleteHouseKeepingItem(id: any){
	   
	   var req_url=globals.HOUSEKEEPING_DELETE_ITEM_URL+"?id="+this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}

	

	getHouseKeepingItem(service_id: any, item_type: any)
	{  var req_url="http://" +globals.apiBaseurl+ "/aboutstays/housekeepingItem/fetchByType?hkServicesId="  + service_id + "&type=" + item_type;
	   return this.apiGet(req_url);	
	}


	getLaundryService(){
	   
	   var req_url="http://" +globals.apiBaseurl+ "/aboutstays/laundry/get?id=" + this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}

	_deleteLaundryItem(id: any){
	   
	   var req_url=globals.LAUNDRY_DELETE_ITEM_URL+"?id="+this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}

	

	getLaundryServiceItem(service_id: any, item_type: any)
	{  var req_url=globals.LAUNDRY_SERVICE_GET_ITEM+"?laundryServicesId="  + service_id + "&laundryType=" + item_type;
	   return this.apiGet(req_url);	
	}

	getInternetService()
	{  var req_url="http://" +globals.apiBaseurl+ "/aboutstays/internetService/get?id="  + this.loginDetails.hotelId;
	   return this.apiGet(req_url);	
	}

	getComplementryService()
	{
		var req_url="http://" +globals.apiBaseurl+ "/aboutstays/complementaryServices/getById?id="  + this.loginDetails.hotelId;
	   return this.apiGet(req_url);	
	}
	getEntertainmentService()
	{
		var req_url="http://" +globals.apiBaseurl+ "/aboutstays/entItem/getAll?entServiceId="  + this.loginDetails.hotelId;
	   return this.apiGet(req_url);	
	}
	loadCommuteServices()
	{	
		var req_url="http://" +globals.apiBaseurl+ "/aboutstays/commuteService/get?id="  + this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}	
}