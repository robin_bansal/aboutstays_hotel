import { Injectable, Inject } from '@angular/core';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { CommonServices } from '../services/common.services'
import * as globals from '../globals';

@Injectable()

export class AmenitiesService extends CommonServices
{
	loginDetails:any;
	constructor (_http: Http) {
		super(_http);
		this.loginDetails=JSON.parse(localStorage['loginDetails']);
	}

	loadAmenities(){
	   
	   var req_url=globals.RESERVATION_CATEGORY_GET;
	   return this.apiGet(req_url);
	}
	
	updateAmenities(data: any){
	   
	   var req_url=globals.RESERVATION_CATEGORY_UPDATE_PATCH;
	   return this.apiPatch(req_url, data);
	}
	addAmenities(data: any){
	   
	   var req_url=globals.RESERVATION_CATEGORY_ADD_POST;
	   return this.apiPost(req_url, data);
	}  	
}