import { Injectable, Inject } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { CommonServices } from '../services/common.services'
import * as globals from '../globals';

@Injectable()

export class MessageService extends CommonServices
{
	
	loginDetails:any;
	
	constructor (public _http: Http) {
		super(_http);
		this.loginDetails=this.getLoginDetails();
	}

	loadMessages(){
	   
	   var req_url=globals.MESSAGES_GET_ALL;
	   return this.apiGet(req_url);
	}
	loadStayMessages(data: any){
		var req_url=globals.MESSAGES_POST_BY_STAY;
	   return this.apiPost(req_url, data);	
	}
	sendMessage(data: any){
		var req_url=globals.MESSAGES_POST_SEND;
	   return this.apiPost(req_url, data);	
	}

	loadChatUsers(){
	   
	   var req_url=globals.CHAT_USERS_GET_GET_BY_HOTEL+"?hotelId="+this.loginDetails.hotelId;
	   return this.apiGet(req_url);
	}
}