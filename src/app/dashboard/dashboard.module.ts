import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';

import { routing } from './dashboard.routes';

@NgModule({
	imports: [routing],
	declarations: [DashboardComponent],
	bootstrap: [DashboardComponent]
})

export class DashboardModule{  }