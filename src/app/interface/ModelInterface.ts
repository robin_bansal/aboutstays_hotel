export interface ModelInterface
{
	loadModel(data: any): void;
	getAttributes(): void;
	setAttributes(arg1: any): void;
	delete(index: number): void;
	updateByIndex(data: any, index: number): void;
	save(obj: Object): void;
}