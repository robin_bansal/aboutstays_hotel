import { NgModule } from '@angular/core';
import { AmenityFilterPipe } from '../pipes/amenityFilter.pipe';

@NgModule({
	declarations: [AmenityFilterPipe],
	exports: [AmenityFilterPipe]
})

export class BaseModule{  }