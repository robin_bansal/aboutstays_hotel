import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from '../settings/settings.component';


export const routes: Routes = [
  { path: '', component: SettingsComponent,
    children: [
    {path: 'hotelinfo', loadChildren: 'app/setting.hotelinfo/setting.hotelinfo.module#HotelinfoModule'},
    {path: 'floormap', loadChildren: 'app/setting.floormap/setting.floormap.module#SettingFloormapModule'},
    {path: 'amenities', loadChildren: 'app/setting.amenities/setting.amenities.module#SettingAmenitiesModule'},
    {path: 'messages', loadChildren: 'app/setting.messages/setting.messages.module#SettingMessageModule'},
    {path: 'servicemenu', loadChildren: 'app/setting.servicemenus/setting.servicemenu.module#ServiceMenuModule'}
    ]
  }
  
  
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);