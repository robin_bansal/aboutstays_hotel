import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { SettingsComponent } from './settings.component';
import { routing } from './setting.routes';
@NgModule({
	imports: [routing],
	declarations: [SettingsComponent],
	bootstrap: [SettingsComponent],
})

export class SettingsModule{  }