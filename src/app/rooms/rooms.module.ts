import { NgModule } from '@angular/core';

import { RoomsComponent } from './rooms.component';
import { routing } from './rooms.routes';

@NgModule({
	imports: [routing],
	declarations: [RoomsComponent],
	bootstrap: [RoomsComponent]
})

export class RoomsModule{  }