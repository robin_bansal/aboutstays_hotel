import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

@Component({
  selector: 'my-app',
  templateUrl: './rooms.html',
  styleUrls: ['css/rooms.css']
})

export class RoomsComponent {}