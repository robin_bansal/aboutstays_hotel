import { NgModule } from '@angular/core';
import {routing} from './inventory.routes';
import { InventoryComponent } from './inventory.component';


@NgModule({
	imports: [routing],
	declarations: [InventoryComponent],
	bootstrap: [InventoryComponent]
})

export class InventoryModule{  }