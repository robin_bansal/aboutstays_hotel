import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

@Component({
  selector: 'my-app',
  templateUrl: './inventory.html',
  styleUrls: ["css/inventory.css"]
})

export class InventoryComponent {}