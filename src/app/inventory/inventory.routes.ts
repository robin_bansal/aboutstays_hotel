import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryComponent } from './inventory.component';


export const routes: Routes = [
  { path: '',  component: InventoryComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);