import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'amenityfilter',
    pure: false
})
export class AmenityFilterPipe implements PipeTransform {
    transform(items: any[], filter: Object): any {
        return items.filter(item => item.type == filter);
        
    }
}