import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingHotelinfoComponent } from './setting.hotelinfo.component';


export const routes: Routes = [
  { path: '',  component: SettingHotelinfoComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);