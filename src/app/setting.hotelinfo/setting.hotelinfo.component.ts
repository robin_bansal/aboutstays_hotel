import { Component, Input, OnInit,trigger, state, style, transition, animate, ElementRef, ViewChild } from '@angular/core';
import {Http} from '@angular/http';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { NgForm, FormGroup, NgModel }                 from '@angular/forms';
import { Observable }       from 'rxjs/Observable';
import { HotelInfoService } from '../services/hotelinfo.service';
import * as globals from '../globals';

@Component({
  templateUrl: './setting.hotelinfo.html',
  styleUrls: ["css/setting.css"]
})

export class SettingHotelinfoComponent extends HotelInfoService {
	@Input() accord: Array<any>;
  @ViewChild('logoInput') inputEl: ElementRef;
  	@Input() hotelinfo: any={
        "hotelId": "",
        "hotelType": '',
        "generalInformation": {
          "address": {
            "addressLine1": "",
            "addressLine2": "",
            "latitude": "",
            "longitude": "",
            "city": "",
            "state": "",
            "pincode": "",
            "country": "",
            "description": ""
          },
          "phone1": "",
          "phone2": "",
          "email": "",
          "fax": "",
          "logoUrl": "",
          "name": "",
          "description": "",
          "checkInTime": "",
          "checkOutTime": "",
          "imageUrl": "",
          "additionalImageUrls": ''
        },
        "roomsIds": [],
        "reviewsAndRatings": '',
        "essentials": [],
        "partnershipType": '',
        "hotelOtherInfo": '',
        "reservationServicePojo": {
          "id": "",
          "typeInfoList": [
            {
              "type": '',
              "typeName": "",
              "imageUrl": "",
              "subTypes": [],
              "imageListing": ''
            },
            {
              "type": '',
              "typeName": "",
              "imageUrl": "",
              "subTypes": [],
              "imageListing": false
            },
            {
              "type": '',
              "typeName": "",
              "imageUrl": "",
              "subTypes": [],
              "imageListing": false
            },
            {
              "type": '',
              "typeName": "",
              "imageUrl": "",
              "subTypes": [],
              "imageListing": false
            },
            {
              "type": '',
              "typeName": "",
              "imageUrl": "",
              "subTypes": [],
              "imageListing": false
            }
          ]
        },
        "timezone": '',
        "amenities": []
    };


    
    public houseRuleCatFormData={title: '', listChildCategory: Array()};
    public houseRuleFormData={title: '', description: ''};

    public houseRules=[
      {
        "id": "",
        "listPGItems": [
            {
              "title": "",
              "listChildCategory": [
                {
                  "title": "",
                  "description": ""
                }
              ]
            }
         ]
      }
    ]
    public tempdata: any;
    public amenityApiStatus=false;
    public hruleApiStatus=false;
    public awardFormData: any;
    public localtyFormData: any;
    
    public loyalties: any;
    public hInfoField: any;  
    public hLocContField: any;
    public awderrors={
      name: {
        error: false,
        message: ''
      },
      year: {
        error: false,
        message: ''
      }
    }
    

	constructor(public _http: Http){
    super(_http);
		this.accord=[];
    this.awardFormData={award: "", year: ""};
    this.localtyFormData={loyaltyName: ''};
    

    this.loyalties=[
      {
        loyaltyId: 1,
        loyaltyName: 'Test Loyalty 1'
      },
      {
        loyaltyId: 2,
        loyaltyName: 'Test Loyalty 2'
      }
    ]

	}
  
  	
  ngOnInit(): void {
    this.startLoader();
    this.getHotelInfoById(globals.hotelID)
    		.subscribe(
    			res => this.initHotelInfo(res),
    			error => this.throwError(error),
    			() => {this.stopLoader();}
    			);

        //get master amenities
        // o means all
    this.loadMasterAmenities(0)
          .subscribe(
            res => this.initAmenities(res),
            error => console.log(error),
            () => {}
            );

  }


  amenitiesOpen()
  {
    //it calls when amenities accordance open
  }

  houseRuleOpen()
  {
    if(!this.hruleApiStatus)
    {
     this.getHouseRules()
        .subscribe(
          res => this.initHouseRules(res),
          error => console.log(error),
          () => {}
          ); 
    }
  }
  initHouseRules(res: any)
  {
    this.houseRules=JSON.parse(JSON.stringify(res)).data;
    this.hruleApiStatus=true;
  }


  initAmenities(res: any)
  {
    this.masterAmeneties=res.data.amenities;
    this.amenityApiStatus=true;
    //console.log(this.masterAmeneties);
  }

  initHotelInfo(res: any)
  {
    this.hotelinfo=JSON.parse(JSON.stringify(res)).data;
    //console.log(JSON.parse(JSON.stringify(res.data)))
    this.hInfoField={
        hotelName: this.hotelinfo.generalInformation.name,
        hotelDesc: this.hotelinfo.generalInformation.description,
        rating: 0,
        checkInTime: this.hotelinfo.generalInformation.checkInTime,
        checkOutTime: this.hotelinfo.generalInformation.checkOutTime,
      };

      this.hLocContField={
        addressLine1: this.hotelinfo.generalInformation.address.addressLine1,
        addressLine2: this.hotelinfo.generalInformation.address.addressLine2,
        city: this.hotelinfo.generalInformation.address.city,
        country: this.hotelinfo.generalInformation.address.country,
        mapLat: this.hotelinfo.generalInformation.address.latitude,
        mapLong: this.hotelinfo.generalInformation.address.logitude,
        email: this.hotelinfo.generalInformation.email,
        fax: this.hotelinfo.generalInformation.fax,
        phone1: this.hotelinfo.generalInformation.phone1,
        phone2: this.hotelinfo.generalInformation.phone2,
        website: 'www.test.com'
      };
  }


 

  

  addAwardFormSubmit(awardFormData: any, event:Event, addawardform: NgForm)
  {
    event.preventDefault();
    //console.log(data);
    var errorflag=false;
    if(awardFormData.award.trim()=="" || awardFormData.award==null || awardFormData.award==undefined)
    {
      this.awderrors.name.error=true;
      this.awderrors.name.message="This field is required";
      errorflag=true;
    }else
    {
      this.awderrors.name.error=false;
    }
    if(awardFormData.year.trim()=="" || awardFormData.year==null || awardFormData.year==undefined)
    {
      this.awderrors.year.error=true;
      this.awderrors.year.message="This field is required";
      errorflag=true;
    }else
    {
      this.awderrors.year.error=false;
    }

    if(errorflag==true)
    {
      return false;
    }
    if(this.hotelinfo.listAwards==null)
    {
      this.hotelinfo.listAwards=[];
    }

    //find award details from mster amenity
    var addedawddetails: any;
    this.masterAmeneties.forEach(function(value: any, index: any, array: any){
      if(value.amenityId==awardFormData.award)
      {
         addedawddetails=value;
      }
    })

    this.hotelinfo.listAwards.push({
        'award': {
          'awardId': awardFormData.award,
          'name': addedawddetails.name,
          'type': 4,
          'iconUrl': addedawddetails.iconUrl
        },
        'year': awardFormData.year,
    });

    //save award on sersver using api
    var data={
      "hotelId": globals.hotelID,
      "awardData": {
        'awardId': awardFormData.award,
        'year': awardFormData.year
      }
    };
    this.startLoader();
    this.updateAward(data)
      .subscribe(
          res => console.log(res),
          error => console.log(error),
          () => {this.stopLoader();}
          );

    //console.log(this.awards);
    addawardform.reset();
    return true;
  }

  removeAward(awd: any)
  {
    var conf=confirm("Are you sure to remove this Award?");
    if(conf==true)
    {
      var index=this.hotelinfo.listAwards.indexOf(awd);
      if(index > -1) {
        this.hotelinfo.listAwards.splice(index, 1);
      }
    }
  }

  saveHotelInfo()
  {
    this.hotelinfo.generalInformation.name=this.hInfoField.hotelName;
    this.hotelinfo.generalInformation.description=this.hInfoField.hotelDesc;
    this.hotelinfo.generalInformation.checkInTime=this.hInfoField.checkInTime;
    this.hotelinfo.generalInformation.checkOutTime=this.hInfoField.checkOutTime;

    //update using api
    this.startLoader();

    var data=Object.assign(this.hotelinfo, {'type': 'hotelID', 'value': globals.hotelID});
    this.updateHotelGeneralInfo(data)
        .subscribe(
          res => console.log(res),
          error => console.log(error),
          () => {this.stopLoader();}
          );

    return true;
  }

  saveLocationContact()
  {
    this.hotelinfo.generalInformation.address.addressLine1=this.hLocContField.addressLine1;
    this.hotelinfo.generalInformation.address.addressLine2=this.hLocContField.addressLine2;
    this.hotelinfo.generalInformation.address.city=this.hLocContField.city;
    this.hotelinfo.generalInformation.address.country=this.hLocContField.country;
    this.hotelinfo.generalInformation.address.latitude=this.hLocContField.mapLat;
    this.hotelinfo.generalInformation.address.longitude=this.hLocContField.mapLong;
    this.hotelinfo.generalInformation.email=this.hLocContField.email;
    this.hotelinfo.generalInformation.fax=this.hLocContField.fax;
    this.hotelinfo.generalInformation.phone1=this.hLocContField.phone1;
    this.hotelinfo.generalInformation.phone2=this.hLocContField.phone2;

    //update using api
    this.startLoader();

    var data=Object.assign(this.hotelinfo, {'type': 'hotelID', 'value': globals.hotelID});
    this.updateHotelGeneralInfo(data)
        .subscribe(
          res => console.log(res),
          error => console.log(error),
          () => {this.stopLoader();}
          );
    return true;
  }

  addLoyaltyFormSubmit(event: Event, form: NgForm)
  {
    event.preventDefault();
    if(this.localtyFormData.loyaltyName =="" || this.localtyFormData.loyaltyName==null)
    {
      return false;
    }
    this.loyalties.push({loyaltyName: this.localtyFormData.loyaltyName});
    //console.log(this.awards);
    this.localtyFormData.loyaltyName="";
    return true;
  }

  removeLoyalty(lty: any)
  {
    var conf=confirm("Are you sure to remove this?");
    if(conf==true)
    {
      var index=this.loyalties.indexOf(lty);
      if(index > -1) {
        this.loyalties.splice(index, 1);
      }
    }
  }

  checkAmenitySelected(amenityId: any)
  {
    var cls="";
    var hotelAmenities=this.hotelinfo.amenities;

    hotelAmenities.forEach(function(value: any, index: any, array: any){
      if(value.amenityId==amenityId)
      {
        cls = "active";
      }
    });
      return cls;
  }
  toggleAmenityStatus(amenity: any)
  {
    var found=false;
    var temp=Array();
    var hotelAmenities=this.hotelinfo.amenities==null?[]:this.hotelinfo.amenities;

    hotelAmenities.forEach(function(value: any, index: any, array: any){
      if(value.amenityId==amenity.amenityId)
      {
        found=true;
      }else
      {
        temp.push(value);
      }
    });
    if(found)
    {
      this.hotelinfo.amenities=temp;
      return "";
    }else
    {
      if(this.hotelinfo.amenities==null)
      {
        this.hotelinfo.amenities=[amenity];
      }else
      {
        this.hotelinfo.amenities.push(amenity);
      }
      return "active";
    }
  }
  saveAmenity()
  {
    var data={
      "hotelId": globals.hotelID,
      "amenityIds": Array()
    };
    if(this.hotelinfo.amenities==null)
    {
      this.hotelinfo.amenities=[];
    }

    this.hotelinfo.amenities.forEach(function(value: any, index: any, array: any){
      data.amenityIds.push(value.amenityId);
    })

    this.startLoader();
    this.updateAmenity(data)
      .subscribe(
          res => console.log(res),
          error => console.log(error),
          () => {this.stopLoader();}
          );
  }
  
///essentials manipulation
  checkEssentialSelected(essentialID: any)
  {
    var cls="";

    var hotelEssentials=this.hotelinfo.essentials==null?[]:this.hotelinfo.essentials;
    
    hotelEssentials.forEach(function(value: any, index: any, array: any){
      if(value.essentialId==essentialID)
      {
        cls = "active";
      }
    });
    return cls;
  }
  toggleEssentialStatus(essential: any)
  {
      var found=false;
      var temp=Array();
      var hotelEssentials=this.hotelinfo.essentials==null?[]:this.hotelinfo.essentials;
      
      
      hotelEssentials.forEach(function(value: any, index: any, array: any){
        if(value.essentialId==essential.amenityId)
        {
          found=true;
        }else
        {
          temp.push(value);
        }
      });
      if(found)
      {
        this.hotelinfo.essentials=temp;
        return "";
      }else
      {
        if(this.hotelinfo.essentials==null)
        {
          this.hotelinfo.essentials=[{
             'name': essential.name,
             'iconUrl': essential.iconUrl,
             'type': essential.type,
             'essentialId': essential.amenityId
           }];
          
        }else
        {
           this.hotelinfo.essentials.push({
             'name': essential.name,
             'iconUrl': essential.iconUrl,
             'type': essential.type,
             'essentialId': essential.amenityId
           });
        }


        return "active";
      }
  }
  saveEssential()
  {
    var data={
      "hotelId": globals.hotelID,
      "essentialsIds": Array()
    };
    if(this.hotelinfo.essentials==null)
    {
      this.hotelinfo.essentials=[];
    }

    this.hotelinfo.essentials.forEach(function(value: any, index: any, array: any){
      data.essentialsIds.push(value.essentialId);
    })

    this.startLoader();
    this.updateEssential(data)
      .subscribe(
          res => console.log(res),
          error => console.log(error),
          () => {this.stopLoader();}
          );
  }
  /////


  //card accepted manipulation
  checkCardSelected(cardID: any)
  {
    var cls="";

    var hotelCardAccepted=this.hotelinfo.listCardAccepted==null?[]:this.hotelinfo.listCardAccepted;
    
    hotelCardAccepted.forEach(function(value: any, index: any, array: any){
      if(value.cardId==cardID)
      {
        cls = "active";
      }
    });
    return cls;
  }
  toggleCardStatus(card: any)
  {
      var found=false;
      var temp=Array();
      var hotelCardAccepted=this.hotelinfo.listCardAccepted==null?[]:this.hotelinfo.listCardAccepted;
      
      
      hotelCardAccepted.forEach(function(value: any, index: any, array: any){
        if(value.cardId==card.amenityId)
        {
          found=true;
        }else
        {
          temp.push(value);
        }
      });
      if(found)
      {
        this.hotelinfo.listCardAccepted=temp;
        return "";
      }else
      {
        if(this.hotelinfo.listCardAccepted==null)
        {
          this.hotelinfo.listCardAccepted=[{
             'name': card.name,
             'iconUrl': card.iconUrl,
             'type': card.type,
             'cardId': card.amenityId
           }];
          
        }else
        {
           this.hotelinfo.listCardAccepted.push({
             'name': card.name,
             'iconUrl': card.iconUrl,
             'type': card.type,
             'cardId': card.amenityId
           });
        }


        return "active";
      }
  }
  saveCardAccepted()
  {
    var data={
      "hotelId": globals.hotelID,
      "cardIds": Array()
    };
    if(this.hotelinfo.listCardAccepted==null)
    {
      this.hotelinfo.listCardAccepted=[];
    }

    this.hotelinfo.listCardAccepted.forEach(function(value: any, index: any, array: any){
      data.cardIds.push(value.cardId);
    })

    this.startLoader();
    this.updateCardAccepted(data)
      .subscribe(
          res => console.log(res),
          error => console.log(error),
          () => {this.stopLoader();}
          );
  }
  ////

  houseruleObjectUpdate()
  {
      this.startLoader();

      var data=this.houseRules;

      this.updateHouseRule(data)
          .subscribe(
            res => console.log(res),
            error => console.log(error),
            () => {this.stopLoader();}
            );
  }

  deleteHouseRuleCat(hrule: any)
  {
    var deleteConf=confirm("Are you sure to delete this?")
    if(deleteConf)
    {
      var index=this.houseRules['listPGItems'].indexOf(hrule);
      if(index > -1) {
        this.houseRules['listPGItems'].splice(index, 1);

        this.houseruleObjectUpdate();
      }
    }
  }

  addHouseRuleCatFormSubmit(event: Event)
  {
      event.preventDefault();
      //console.log(data);
      var errorflag=false;
      
      if(this.houseRuleCatFormData.title.trim()=="" || this.houseRuleCatFormData.title==null)
      {
        errorflag=true;
      }
      

      if(errorflag==true)
      {
        return false;
      }

      this.houseRules['listPGItems'].push({title: this.houseRuleCatFormData.title, listChildCategory: []});

      this.houseruleObjectUpdate();

      //console.log(this.awards);
      this.houseRuleCatFormData.title="";
      return true;
  }

  addHouseRuleFormSubmit(event: Event, houseRuleCatId: any)
  {
      event.preventDefault();
      //console.log(data);
      var errorflag=false;
      
      if(this.houseRuleFormData.title.trim()=="" || this.houseRuleFormData.title==null)
      {
        errorflag=true;
      }
      

      if(errorflag==true)
      {
        return false;
      }
      var frmtitle=this.houseRuleFormData.title;
      var frmdesc=this.houseRuleFormData.description;
      this.houseRules['listPGItems'].forEach(function(value: any, index: any, array: any){
        if(value.title==houseRuleCatId)
        {
          value.listChildCategory.push({title: frmtitle, description: frmdesc});
        }
      });

      this.houseruleObjectUpdate();

      //console.log(this.awards);
      this.houseRuleFormData.title="";
      this.houseRuleFormData.description="";
      return true;
  }
  deleteHouseRule(hrule: any, hsrule: any)
  {
    var deleteConf=confirm("Are you sure to delete this?")
    if(deleteConf)
    {
      var index=this.houseRules['listPGItems'].indexOf(hrule);
      if(index > -1) {
        var subindex=this.houseRules['listPGItems'][index].listChildCategory.indexOf(hsrule);
        if(subindex>-1)
        {
          this.houseRules['listPGItems'][index].listChildCategory.splice(subindex, 1);
          this.houseruleObjectUpdate();
        }
      }

      
    }
  }

  uploadLogo()
  {
    let inputEl: HTMLInputElement = this.inputEl.nativeElement;
        let formData = new FormData();
        formData.append('file', inputEl.files.item(0));
            
            //call upload image api
          this.startLoader();

        
        this.uploadFile(formData)
        .subscribe(
          res => console.log(res),
          error => console.log(error),
          () => {this.stopLoader();}
          );          
  }
}