import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule }    from '@angular/forms';
import { SettingHotelinfoComponent } from './setting.hotelinfo.component';
import { BaseModule } from '../modules/base.module';

import {routing} from './hotelinfo.router';

@NgModule({
	imports: [HttpModule, FormsModule, routing, CommonModule, BaseModule],
	declarations: [SettingHotelinfoComponent],
	bootstrap: [SettingHotelinfoComponent]
})

export class HotelinfoModule{  }