import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';


export const routes: Routes = [
  { path: 'settings',  redirectTo: 'settings/hotelinfo', pathMatch: 'full'},
  {path: 'settings', loadChildren: 'app/settings/settings.module#SettingsModule'},
  {path: 'dashboard', loadChildren: 'app/dashboard/dashboard.module#DashboardModule'},
  { path: '',  redirectTo: 'dashboard', pathMatch: 'full'},

  { path: 'rooms', loadChildren: 'app/rooms/rooms.module#RoomsModule' },
  { path: 'arr-dep', loadChildren: 'app/arrdep/arrdep.module#ArrdepModule' },
  { path: 'crew', loadChildren: 'app/crew/crew.module#CrewModule' },
  { path: 'deals', loadChildren: 'app/deals/deals.module#DealsModule' },
  { path: 'inventory', loadChildren: 'app/inventory/inventory.module#InventoryModule' },
  { path: 'messages', loadChildren: 'app/messages/messages.module#MessagesModule' },
  { path: 'requests', loadChildren: 'app/requests/requests.module#RequestsModule' },
];


export const routing: ModuleWithProviders=RouterModule.forRoot(routes);