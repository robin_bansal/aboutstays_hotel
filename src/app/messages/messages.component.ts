import { Component, OnInit } from '@angular/core';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';

import * as io from 'socket.io-client';

import { MessageService } from './message.service';
import { CommonServices } from '../services/common.services';


import { MessageModel, ChatUsersModel, SocketMessage } from './response';
import * as globals from '../globals';

@Component({
  selector: 'my-app',
  templateUrl: './messages.html',

  styleUrls: ["../../css/message.css"],
  providers : [MessageService,CommonServices]
})

export class MessagesComponent extends CommonServices implements OnInit{

	
	public messages : MessageModel[];
	public chatUsers : ChatUsersModel[];
	public selectedUser: ChatUsersModel;

	public newMessage : any;
	public newMessage1 : any  ;
	public socketMessageSend : any;
	public socketMessageReceive : SocketMessage;
	public addSocketTemp : any;
	public message : String;
	public now = new Date();
	connection;
	socketUrl = "http://" + globals.socketBaseurl;
	private socket;

	constructor(public messageService : MessageService, public _http:Http){
		super( _http );
		this.messages = [];
		this.newMessage1 = {};
		this.newMessage = {};
		this.socketMessageSend = {};
		this.addSocketTemp = {};
		this.socketMessageSend.metadata = {};	
						
					
	}

	

	sendMessageBySocket(message : String){
	
		this.socketMessageSend.socketType = 2;
		this.socketMessageSend.fromId = globals.hotelID;
		this.socketMessageSend.toId = this.selectedUser.staysData.userId;
		this.socketMessageSend.messageId = this.now.getTime();
		this.socketMessageSend.metadata.text = message;
		this.socketMessageSend.metadata.stayId = this.selectedUser.staysData.staysId;
		console.log(this.socketMessageSend);


		this.messageService.sendMessageSocket(this.socketMessageSend);

	}
	
	ngOnInit(): void{
	
		
		this.messageService.getChatUsers().subscribe( 

			response => {
				
				console.log(response.data),
	  			this.chatUsers = response.data, 
	            this.selectedUser=this.chatUsers[0],
	            this.fetchMessage(this.selectedUser),
	          
	            this.addSocketTemp.socketType = 1;
		        this.addSocketTemp.hotelID = globals.hotelID;
		  
		        this.messageService.addSocket( this.addSocketTemp ),
		        this.socket = io(this.socketUrl),
		        this.socket.on('receive', function (data) {
		        	console.log(data);
		        	this.socketMessageReceive = data;
				    if( this.socketMessageReceive.socketType == 2 && this.socketMessageReceive.metadata.stayId == this.selectedUser.staysData.userId ){
			    		
						this.newMessage1.messageText=String(this.socketMessageReceive.metadata.text);
						this.newMessage1.userId=this.socketMessageReceive.fromId;
						this.newMessage1.staysId=this.socketMessageReceive.metadata.stayId;
						this.newMessage1.hotelId=this.socketMessageReceive.fromId;
						this.newMessage1.status=1;
						this.newMessage1.messageId="";
						this.newMessage1.byUser=true;
						this.newMessage1.created=this.now.getTime();
						
						this.messages.push(this.newMessage1);
					}
			   	}.bind(this));
		    	


        	}
            //error => this.throwError(error),
            //() => {}
        );

	}

	ngOnDestroy(){
		this.connection.unsubscribe();
	}


	chatUserTrigger(user: any){
		this.selectedUser=user;
		this.fetchMessage(user);
	}

	fetchMessage(user: any)
	{

		console.log(1);	

		let data={
			"hotelId": globals.hotelID,
		    "userId": user.staysData.userId,
		    "staysId": user.staysData.staysId
		};

		this.messageService.getUserMessages(data).subscribe( response => {
            	if(response.error==false){
            		
            		this.messages = response.data.messages;

            	}
            }	
        );
	}

	submitMessage1(value: any, valid:boolean)
	{
		let message=value.message;
		if(valid){
			
			let now= new Date();
			
			this.newMessage.messageText=String(message);
			this.newMessage.userId=this.selectedUser.staysData.userId;
			this.newMessage.staysId=this.selectedUser.staysData.staysId;
			this.newMessage.hotelId=this.selectedUser.staysData.staysId;
			this.newMessage.status=1;
			this.newMessage.messageId="";
			this.newMessage.byUser=true;
			this.newMessage.created=now.getTime();
			
			this.messageService.sendMessage(this.newMessage).subscribe(res => {
	            	if(res.error==false){
	            		this.newMessage.messageId=res.data;
	            		this.messages.push(this.newMessage);	            	
	            } 
	           }),
	           () => {};
		}
	}

	submitMessage(form : any, value: any, valid:boolean){
		
		if(valid){
			console.log(value);
			let text = value.message;
			form.reset();
			console.log(text);
			let now= new Date();
			this.newMessage = {};
			this.newMessage.messageText=String(text);
			this.newMessage.userId=this.selectedUser.staysData.userId;
			this.newMessage.staysId=this.selectedUser.staysData.staysId;
			this.newMessage.hotelId=this.selectedUser.staysData.staysId;
			this.newMessage.status=1;
			this.newMessage.messageId="";
			this.newMessage.byUser=true;
			this.newMessage.created=now.getTime();
			console.log(this.newMessage);
			this.messages.push(this.newMessage);
			console.log(this.messages);
			this.sendMessageBySocket(String(this.newMessage.messageText));
		}

	}




}