
export class MessageModel{
    "messageText": String;
    "userId": "";
    "staysId": "";
    "hotelId": "";
    "status": number;
    "messageId": "";
    "byUser": boolean;
    "created": number;
}

export class ChatUsersModel{
	"userData": {
	    "title": "";
	    "firstName": "";
	    "lastName": "";
	    "imageUrl": "";
	    "emailId": "";
	    "number": "";
	    "gender": "";
	    "userId": "";
	    "dob": "";
	    "anniversaryDate": "";
	    "maritalStatus": 2;
	    "homeAddress": {
	        "addressLine1": "";
	        "addressLine2": "";
	        "latitude": "";
	        "longitude": "";
	        "city": "";
	        "state": "";
	        "pincode": "";
	        "country": "";
	        "description": "";
	    };
	    "officeAddress": {
	        "addressLine1": "";
	        "addressLine2": "";
	        "latitude": "";
	        "longitude": "";
	        "city": "";
	        "state": "";
	        "pincode": "";
	        "country": "";
	        "description": "";
	    };
	    "listStayPreferences": "";
	    "listIdentityDocs": "";
	};
    "staysData": {
        "staysId": "";
        "bookingId": "";
        "hotelId": "";
        "roomId": "";
        "userId": "";
        "checkInDate": "";
        "checkInTime": "";
        "checkOutTime": "";
        "checkOutDate": "";
        "official": false;
        "staysOrdering": 0;
    };
    "unreadMessageCount": 0;
};

export class SocketMessage{
	"socketType": number;
	"toId" : "";
	"fromId" : "";
	"messageId" : "";
	"metadata" : {
		"text" : "";
		"stayId" : "";
	};
}