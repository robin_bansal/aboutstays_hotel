import { Injectable } from '@angular/core';
import { Http, Request, RequestMethod, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map' ;
import * as io from 'socket.io-client';
import {Observable } from 'rxjs/Observable'

import * as globals from '../globals';


@Injectable()
export class MessageService {

	constructor( private http: Http) {}

	url :String = "http://" + globals.apiBaseurl + "/aboutstays";
	bookingDate = "05-07-2017";
	headers = new Headers({'Content-Type': 'application/json'});
	socketUrl = "http://" + globals.socketBaseurl;
	private socket = io(this.socketUrl);


	getChatUsers( ){
		const api = `${this.url}/message/getChatUsers?hotelId=${globals.hotelID}`;
		return this.http.get(api).map( 
			response => response.json() 
		);
	}

	getUserMessages( data ){
		const api = `${this.url}/message/getStayNotifications`;
		return this.http.post(api,JSON.stringify(data),{headers:this.headers}).map(
			response => response.json()
		);
	}

	getAllMessages( ){
		const api = `${this.url}/message/getAll`;
		return this.http.get(api).map( 
			response => response.json() 
		);

	}

	sendMessage( data ){
		const api = `${this.url}/message/add`;

		return this.http.post(api,JSON.stringify(data),{headers:this.headers}).map(
			response => response.json()
		);
	}


	addSocket( data ): void{
		const api = `${this.socketUrl}`;
		console.log(data);

		this.socket.emit('addSocket', data );
		console.log(2);
	}

	getMessagesSocket(){
		let observable = new Observable( observer =>{
			
			this.socket.on('receive',(data) => {
				 observer.next(data);

			});
			return () => {
				this.socket.disconnect();
			}

		} );
	}

	sendMessageSocket(messageObj){
		const api = `${this.socketUrl}`;

		console.log(messageObj);
	

	}

}