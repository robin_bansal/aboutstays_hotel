import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm, FormGroup, Validators, Validator }    from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppRoutingModule } from './messages.routes';
import { MessagesComponent } from './messages.component';


@NgModule({

	imports: [
			CommonModule, 
			HttpModule, 
			AppRoutingModule, 
			FormsModule
		],
	declarations: [
			MessagesComponent
		],
	bootstrap: [
			MessagesComponent
		]

})

export class MessagesModule{  }