import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { HttpModule } from '@angular/http';
import {routing} from './floormap.route';
import { FormsModule }    from '@angular/forms';
import { SettingFloormapComponent } from './setting.floormap.component';
import { BaseModule } from '../modules/base.module';

@NgModule({
	imports: [CommonModule, HttpModule, FormsModule, routing, BaseModule],
	declarations: [SettingFloormapComponent],
	bootstrap: [SettingFloormapComponent]
})

export class SettingFloormapModule{  }