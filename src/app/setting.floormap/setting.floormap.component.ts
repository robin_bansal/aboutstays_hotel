import { Component, OnInit, trigger, Input } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { NgForm }                 from '@angular/forms';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import {RoomCategory, Wing, Floor, Room} from '../models/floormap.model';
import { FloormapService } from '../services/floormap.services';
@Component({
  selector: 'my-app',
  templateUrl: './setting.floormap.html',
  styleUrls: ["css/setting.css"]
})

export class SettingFloormapComponent extends FloormapService {
	public accord: Array<any>;
	public roomcategories = new RoomCategory;
	public wings = new Wing;
	public floors = new Floor;
	public floorsAray: Array<any>;
	public rooms = new Room;
	public apiStatus: Array<any>;
	public roomcatEditStatus: Array<any>;
	public editFloorData: any;
	public roomCatFormData: any;
	public wingFormData=new Wing;
	public floorFormData: Floor;
	public roomFormData=new Room;
	public markRoomsDelete=Array();
	public amenityEditData: any;
	public filteredid: any='';
	
	constructor(public _http: Http){
		super(_http);
		this.accord=Array();
		this.roomcatEditStatus=Array();
		this.apiStatus=Array();
		this.amenityEditData={
			'floor': {},
			'rooms': []
		}
	}

	ngOnInit(): void{
			this.startLoader();
			this.loadRoomCategories()
				.subscribe(
	            res => {this.roomcategories.loadModel(res.data); this.stopLoader()},
	            error => this.throwError(error),
	            () => {}
	            );

	            //get master amenities
    	this.loadMasterAmenities(0)
          .subscribe(
            res => {this.masterAmeneties=res.data.amenities},
            error => this.throwError(error),
            () => {}
            );

        //get wings floors and rooms using api
        this.loadWings()
          .subscribe(
            res => this.wings.loadModel(res.data),
            error => this.throwError(error),
            () => {}
            );


	}

  checkAmenitySelected(amenityId: any, amenities: any)
  {
    var cls="";
    amenities=(amenities==null)?[]:amenities;
    amenities.forEach(function(value: any, index: any, array: any){
      if(value.amenityId==amenityId)
      {
        cls = "active";
      }
    });
    return cls;
  }
  toggleAmenityStatus(amenity: any, amenities: any)
  {
    var found=false;
    var temp=Array();
    amenities=amenities==null?[]:amenities;
    amenities.forEach(function(value: any, index: any, array: any){
      if(value.amenityId==amenity.amenityId)
      {
        found=true;
      }else
      {
        temp.push(value);
      }
    });

    if(!found)
    {
      
      temp.push(amenity);
      
    }
    return temp;

  }

  formAddRoomCategory()
  {
  	var newroomcat=new RoomCategory;
  	this.roomCatFormData = this.roomcatEdit(newroomcat.model[0]);
  	


  	this.roomcategories.save(this.roomCatFormData);
  	this.accord['rc1']=true;
  	this.roomcatEditStatus[1]=true;
  	
  	
  }

	roomcatEdit(roomcat: any)
	{
		var rcat= new RoomCategory;
		rcat.setAttributes(roomcat);
		return rcat.model[0];
	}
	
	saveRoomCategory(data: any, catid: number)
	{
		console.log(JSON.stringify(data));
		this.startLoader();
		if(data.roomCategoryId=="")
		{
			//console.log(JSON.stringify(data));
			//add new to server
			this.addRoomCat(data)
				.subscribe(
	    			res => {res.error==true?this.throwError(res.message):this.roomcategories.roomCatUpdateByCatId(data, catid);this.showAlert("SUCCESS", "Room Category successfuly added")},
	    			error => console.log(error),
	    			() => {this.stopLoader();}
	    			);
		}else
		{
			//update to server
			console.log(JSON.stringify(data));
			this.updateRoomCat(data)
				.subscribe(
	    			res => {res.error==true?this.throwError(res.message):this.roomcategories.roomCatUpdateByCatId(data, catid);this.showAlert("SUCCESS", "Room Category successfuly Updated")},
	    			error => console.log(error),
	    			() => {this.stopLoader();}
	    			);
		}

		
	}

	

	wingAccordOpen()
	{
		this.apiStatus['wing']=false;
		this.loadFloors()
          .subscribe(
            res => {this.floors.loadModel(res.data);},
            error => this.throwError(error),
            () => {}
            );

         this.loadRooms()
          .subscribe(
            res => {this.rooms.loadModel(res.data); this.apiStatus['wing']=true},
            error => this.throwError(error),
            () => {}
            );
	}

	submitAddWing(formdata: any)
	{
		if(formdata.name.trim()=="" || formdata.code.trim()=="")
			return false;
		//formdata.hoteId=

		//adding to server using api
		this.startLoader();
		var wdata={
	        "name": formdata.name,
	        "code": formdata.code,
	        "hotelId": formdata.hotelId
 		};

		this.addWing(wdata)
			.subscribe(
    			res => {res.error==true?this.throwError(res.message):this.wings.save(formdata);this.showAlert("SUCCESS", "Wing successfuly added")},
    			error => console.log(error),
    			() => {this.stopLoader();}
    			);
		


		

		var temp=new Wing;
		this.wingFormData.model=temp.model;
		return true;
	}
	roomAmenityPopupOpen(floorId: any)
	{
		this.amenityEditData.floor=JSON.parse(JSON.stringify(this.floors.getById(floorId)));
		this.amenityEditData.rooms=JSON.parse(JSON.stringify(this.rooms.getByFloorId(floorId)));
	}
	filterRoomAmenity(amenity_id: any)
	{
		return this.masterAmeneties.filter(function(item: any){
			if(amenity_id=="")
				return true;
			return item.amenityId==amenity_id;
		})
	}
	saveAdditionalRoomAmenity()
	{
		this.startLoader();
		var data={
					"dataToUpdate": Array()
				};

		this.amenityEditData.rooms.forEach(function(value: any){
			value.additionalAmenities=value.additionalAmenities==null?[]:value.additionalAmenities;
				var temp={
							"roomId":value.roomId,
							"amentityIds":Array()
						};
				value.additionalAmenities.forEach(function(value1: any){
					temp.amentityIds.push(value1.amenityId);
				});
			data.dataToUpdate.push(temp);
		})
		this.updateRoomsAmenities(data)
			.subscribe(
	            res => {
	            	if(res.error==true)
	            	{
	            		this.throwError(res);
	            	}else
	            	{
	            		//success message
	            		this.stopLoader();
	            	}
	            },
	            error => this.throwError(error),
	            () => {}
	            );

			return true;

	}
	addFloorPopupOpen(wingid: any)
	{
		this.floorFormData=new Floor;
		this.floorFormData.model[0].wingId=wingid;
		this.floorFormData.model[0].hotelId=JSON.parse(localStorage['loginDetails']).hotelId;
	}
	submitAddFloor(formdata: any)
	{
		if(formdata.name.trim()=="" || formdata.code.trim()=="")
			return false

		//adding to server using api
		this.startLoader();
		var fdata={
		        "name": formdata.name,
		        "code": formdata.code,
		        "hotelId": formdata.hotelId,
		        "wingId": formdata.wingId
		    };

		this.addFloor(fdata)
			.subscribe(
    			res => {res.error==true?this.throwError(res.message):this.floors.save(formdata);this.showAlert("SUCCESS", "Floor successfuly added")},
    			error => console.log(error),
    			() => {this.stopLoader();}
    			);

		
		//update count
		this.wings.increaseCount(formdata.wingId, 1, 0);
		//console.log(this.floorFormData.model);
		return true;
	}

	submitAddRoom(formdata: any, editmode: boolean=false)
	{
		if(formdata.roomName.trim()=="" || formdata.roomNumber.trim()=="" || formdata.roomNumber.trim()==0)
			return false;

		//adding to server using api
		this.startLoader();
		var rdata={
	        "roomName": formdata.roomName,
	        "roomNumber": formdata.roomNumber,
	        "wingId": formdata.wingId,
	        "floorId": formdata.floorId,
	        "roomCategoryId": formdata.roomCategoryData.roomCategoryId,
	        "hotelId": formdata.hotelId
	    };
	    
		this.addRoom(rdata)
			.subscribe(
    			res => {
    				
    				if(res.error==true)
    				{
    					this.throwError(res.message);
    				}else
    				{
    					formdata.roomId=res.data.roomId;
    					if(editmode)
					    {
					    	this.editFloorData['rooms'].push(formdata);	
					    }
					    console.log(formdata);
					    
    					this.rooms.save(formdata);this.showAlert("SUCCESS", "Room successfuly added");
    				}
    			},
    			error => console.log(error),
    			() => {this.stopLoader();}
    			);
		
		//update count
		this.wings.increaseCount(formdata.wingId, 0, 1);
		this.floors.increaseCount(formdata.floorId, 1);

		var temp=new Room;
		this.roomFormData.model=temp.model;
		this.editFloorData={"rooms": this.rooms.getByFloorId(formdata.floorId)};
		return true;
	}

	cancelRoomCatSave()
	{
		var temp=new RoomCategory;
		var lastindex=this.roomcategories.getCount()-1;
		
		if(JSON.stringify(this.roomcategories.getLastItem())===JSON.stringify(temp.model))
			this.roomcategories.delete(lastindex);	
		console.log(temp.model);
		
	}

	editFloorEvent(wingid: any, floorid: any)
	{
		var temp=JSON.stringify(this.rooms.getByFloorId(floorid));
		this.editFloorData={"rooms": JSON.parse(temp)};
		this.roomFormData.model[0].floorId=floorid;
		this.roomFormData.model[0].wingId=wingid;
		this.roomFormData.model[0].hotelId=JSON.parse(localStorage['loginDetails']).hotelId;
	}
	saveFloor()
	{

		this.startLoader();
		
		let batchUpdateData={
							"hotelId":this.loginDetails.hotelId,
							"dataToUpdate": Array()
						};
		
		var tempeditdata=this.editFloorData;
		tempeditdata.rooms.forEach(function(value: any, index: number){
			this.rooms.model.forEach(function(value1: any, index1: number){
				if(value.roomId==value1.roomId)
				{
					
					let catid=value.roomCategoryData.roomCategoryId;
					let cat=this.roomcategories.findRoomcatById(catid);
					value.roomCategoryData=cat;

					batchUpdateData.dataToUpdate.push({
									"roomId":value.roomId,
									"roomCategoryId":catid,
									"roomNumber":value.roomNumber,
									"panelUiOrder":index+1
								});

					this.rooms.updateRoom(value);
				}
			}, this)
		},this)

		this.updateRoomsBatch(batchUpdateData)
			.subscribe(
	            res => {
	            	if(res.error==true)
	            	{
	            		this.throwError("Somthing wrong. Please try again later.");
	            	}else
	            	{
	            		//success message
	            		this.stopLoader();
	            	}
	            },
	            error => this.throwError(error),
	            () => {}
	            );
		




	}

	markRoomDeleted(id: any)
	{
		var index=this.markRoomsDelete.indexOf(id);
	    if(index > -1) {
	       this.markRoomsDelete.splice(index, 1);
	    }else
	    {
	    	this.markRoomsDelete.push(id);	
	    }
	    //console.log(this.markRoomsDelete);

	    if(this.markRoomsDelete.length>0)
	    {
	    	return true;
	    }else
	    {
	    	return false;
	    }
	}

	removeMarkedRooms()
	{
		if(this.markRoomsDelete.length>0)
		{
			var editfloordata=this.editFloorData;
			let deleteRoomIDs="";
			this.markRoomsDelete.forEach(function(value:any, index:any, array:any){
				let roomid=editfloordata['rooms'][value].roomId;
				this.rooms.deleteRoomById(roomid);
				

				if(deleteRoomIDs=="")
				{
					deleteRoomIDs += roomid;
				}else
				{
					deleteRoomIDs += "," + roomid;
				}
				editfloordata['rooms'].splice(value, 1);
					
			}, this)
			this.markRoomsDelete=Array();

			this.startLoader();
			this.deleteRooms(deleteRoomIDs)
				.subscribe(
	            res => {this.stopLoader()},
	            error => this.throwError(error),
	            () => {}
	            );
		}
	}
}