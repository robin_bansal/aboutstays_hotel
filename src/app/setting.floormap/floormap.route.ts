import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingFloormapComponent } from './setting.floormap.component';


export const routes: Routes = [
  { path: '',  component: SettingFloormapComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);