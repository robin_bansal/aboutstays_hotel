'use strict';

export const apiBaseurl: String="52.26.27.78:8080";
export const GET_HOTEL_INFO_URL : string = apiBaseurl+"/aboutstays/hotel/get?hotelId="
export const FOOD_ADD_ITEM_URL : string = "http://"+apiBaseurl+"/aboutstays/foodItem/add"
export const HOUSEKEEPING_ADD_ITEM_URL : string = "http://"+apiBaseurl+"/aboutstays/housekeepingItem/add"
export const HOUSEKEEPING_DELETE_ITEM_URL : string = "http://"+apiBaseurl+"/aboutstays/housekeepingItem/delete"
export const LAUNDRY_SERVICE_GET_ITEM : string ="http://"+apiBaseurl + "/aboutstays/laundryItem/getByLaundryType"
export const LAUNDRY_DELETE_ITEM_URL : string = "http://"+apiBaseurl+"/aboutstays/laundryItem/delete"

export const MESSAGES_GET_ALL : string = "http://"+apiBaseurl+"/aboutstays/message/getAll"
export const MESSAGES_POST_BY_STAY : string = "http://"+apiBaseurl+"/aboutstays/message/getStayNotifications"
export const MESSAGES_POST_SEND : string = "http://"+apiBaseurl+"/aboutstays/message/add"
export const CHAT_USERS_GET_GET_BY_HOTEL : string = "http://"+apiBaseurl+"/aboutstays/message/getChatUsers"
export const MASTER_CHANNELS_GET_ALL : string = "http://"+apiBaseurl+"/aboutstays/masterChannel/getAll"

export const ROOM_DELETE_GET : string = "http://"+apiBaseurl+"/aboutstays/room/batch/delete"
export const ROOMS_BATCH_UPDATE_PATCH : string = "http://"+apiBaseurl+"/aboutstays/room/batch/updateMetaInfo"
export const ROOMS_AMENITY_UPDATE_PATCH : string = "http://"+apiBaseurl+"/aboutstays/room/batch/linkAdditionalAmenities"

export const RESERVATION_CATEGORY_GET : string = "http://"+apiBaseurl+"/aboutstays/reservationCategory/getAll"
export const RESERVATION_CATEGORY_UPDATE_PATCH : string = "http://"+apiBaseurl+"/aboutstays/reservationCategory/update"
export const RESERVATION_CATEGORY_ADD_POST : string = "http://"+apiBaseurl+"/aboutstays/reservationCategory/add"


export const hotelID: String="59143728e4b08d3751f8c49f";