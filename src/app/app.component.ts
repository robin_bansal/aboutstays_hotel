import { Component } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

import { LoginDetails } from './models/logindetails.model';
@Component({
  selector: 'my-app',
  templateUrl: `./app.html`
})
export class AppComponent  {
	routerLoading: boolean=false;
	constructor(private router: Router){

	}
	ngOnInit(): void{
		this.router.events
	    .subscribe((event) => {
	      if (event instanceof NavigationStart) {
		      this.routerLoading=true;
		  }
	      if (event instanceof NavigationEnd) {
		   	  this.routerLoading=false;   
		  }
	    });

		var loginDetails = new LoginDetails('59143728e4b08d3751f8c49f');
		
		localStorage.setItem('loginDetails', JSON.stringify(loginDetails));
	}


}
