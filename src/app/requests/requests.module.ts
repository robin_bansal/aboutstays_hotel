import { NgModule } from '@angular/core';
import {routing} from './requests.routes';

import { RequestsComponent } from './requests.component';


@NgModule({
	imports: [routing],
	declarations: [RequestsComponent],
	bootstrap: [RequestsComponent]
})

export class RequestsModule{  }