import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestsComponent } from './requests.component';


export const routes: Routes = [
  { path: '',  component: RequestsComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);