import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

@Component({
  selector: 'my-app',
  templateUrl: './requests.html',
  styleUrls: ["css/request.css"]
})

export class RequestsComponent {}