import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

@Component({
  selector: 'my-app',
  templateUrl: './deals.html',
  styleUrls: ["css/deals.css"]
})

export class DealsComponent {}