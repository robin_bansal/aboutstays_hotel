import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DealsComponent } from './deals.component';


export const routes: Routes = [
  { path: '',  component: DealsComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);