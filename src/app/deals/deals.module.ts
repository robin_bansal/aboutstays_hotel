import { NgModule } from '@angular/core';
import {routing} from './deals.routes'
import { DealsComponent } from './deals.component';


@NgModule({
	imports: [routing],
	declarations: [DealsComponent],
	bootstrap: [DealsComponent]
})

export class DealsModule{  }