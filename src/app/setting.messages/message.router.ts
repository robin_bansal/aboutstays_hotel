import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingMessagesComponent } from './setting.messages.component';


export const routes: Routes = [
  { path: '',  component: SettingMessagesComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);