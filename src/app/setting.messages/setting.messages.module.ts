import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { HttpModule } from '@angular/http';
import {routing} from './message.router';

import { SettingMessagesComponent } from './setting.messages.component';


@NgModule({
	imports: [routing,CommonModule,HttpModule],
	declarations: [SettingMessagesComponent],
	bootstrap: [SettingMessagesComponent]
})

export class SettingMessageModule{  }