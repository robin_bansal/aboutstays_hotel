import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { CommonServices } from '../services/common.services';

@Component({
  selector: 'my-app',
  templateUrl: './setting.messages.html',
  styleUrls: ["css/setting.css"],
  providers: [CommonServices]
})

export class SettingMessagesComponent {
	constructor(private commonservices: CommonServices){

	}
}