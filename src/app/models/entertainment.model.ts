import {BaseModel} from '../models/base.models'
export class EntertainmentServiceModel extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model=[
			{
	            "number": "",
	            "hotelId": "",
	            "serviceId": "",
	            "itemId": "",
	            "itemDetails": {
	                "name": "",
	                "logo": "",
	                "imageUrl": "",
	                "description": "",
	                "channelId": "",
	                "genereDetails": {
	                    "name": "",
	                    "logo": "",
	                    "masterGenereId": ""
	                }
	            }
	        }
		]
	}

	deleteItem(itemId: any)
	{
		let temp=Array();
		this.model.forEach(function(value: any){
			if(value.itemId!=itemId)
			{
				temp.push(value);
			}
		})
		this.model=temp;
	}	

}