import {ModelInterface} from '../interface/ModelInterface';
import * as globals from '../globals';
export class BaseModel implements ModelInterface
{
	public model: any;
	public loginDetails: any;
	constructor(){
		this.loginDetails=JSON.parse(localStorage['loginDetails']);
	}
	loadModel(data: any): void
	{
		this.model=data;
	}

	setAttributes(obj: Object): void
	{
		for(var item in obj)
		{
			this.model[0][item]=obj[item];
		}
		
	}
	save(obj: Object): void
	{
		this.model.push(obj);
	}
	updateByIndex(data: Object, index: number): void
	{
		this.model[index]=data;
	}


	getAttributes()
	{

	}

	delete(index: number): void
	{
		this.model.splice(index, 1);
	}

	getItem(index: number): any
	{
		if(index>=0)
		{
			return this.model[index];
		}else
		{
			return {};
		}
	}
	

	getLastItem(): any
	{
		var len=this.getCount();
		if(len>0)
		{
			return this.model[len-1];
		}else
		{
			return {};
		}
	}

	getCount(): number
	{
		return this.model.length;
	}
	
	one(): any
	{
		return this.model[0];
	}	

}