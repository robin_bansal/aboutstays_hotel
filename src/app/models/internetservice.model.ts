export class InternetServiceModel
{
	public service: any;
	constructor()
	{
		this.service={
				"id": "",
		        "listPacks": [
					{
	                    "title": "",
	                    "description": "",
	                    "mrp": 0,
	                    "price": 0,
	                    "complementry": false,
	                    "category": "",
	                    "showOnApp": false
	                }		            
	            ]
	        }

	}

	getCategory()
	{
		var category=Array();
		if(this.service.listPacks==undefined)
		{
			return [];
		}
		return this.service.listPacks.filter(function(item: any){
			if(category.indexOf(item.category)<0 && item.category!=null && item.category!="")
			{
				category.push(item.category);
				return true;
			}else
			{
				return false;
			}
		})
	
	}

	getCategoryItems(category: any)
	{
		if(this.service.listPacks==undefined)
		{
			return [];
		}
		return this.service.listPacks.filter(function(item: any){
			if(item.category==category && item.category!=null && item.category!="")
			{
				return true;
			}else
			{
				return false;
			}
		})	
	}

	updateAllItem(upitems: any)
	{
		
		upitems.forEach(function(upitem: any, index1: number){

			this.getCategoryItems(upitems[0].category).forEach(function(item:any, index2: number){
				if(index1==index2)
				{
					item.title=upitem.title;
					item.description=upitem.description;
					item.price=upitem.price;
					item.showOnApp=upitem.showOnApp;
				}
			})
			
		}, this)
	}


	addItem(item: any)
	{
		this.service.listPacks.push(item);
	}

	deleteItem(itemObj: any)
	{
		
		let found=-1;
		this.service.listPacks.forEach(function(value: any, index: number){
				if(value==itemObj)
				{
					found=index;
				}
		})

		if(found>=0)
			this.service.listPacks.splice(found, 1);
		
	}
}