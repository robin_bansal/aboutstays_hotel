import {BaseModel} from '../models/base.models'
export class ComplementryServiceModel extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model={
				"servicesId": "",
		        "tabWiseComplementaryServices": [
		            {
		            	"tabName": "",
		            	"services": [
		            		{
		            			"title": "",
		            			"descriptionList": []
		            		}
		            	]
		            }
	            ]
	        }
	}

	findItemByTabAndTitle(tab: string, title: string)
	{
		let returnData: any;
		this.model.tabWiseComplementaryServices.forEach(function(value: any){
			if(value.tabName==tab)
			{
				value.services.forEach(function(value1: any){
					if(value1.title==title)
					{
						returnData=value1;
						return false;
					}
				})
			}
		});

		return returnData;
	}


	deleteItem(tabName: string, title: string, index: number)
	{
		let data=this.findItemByTabAndTitle(tabName, title);
		data.descriptionList.splice(index, 1);
	}

	addItem(tabName: string, title: string, newItem: any)
	{
		let data=this.findItemByTabAndTitle(tabName, title);
		data.descriptionList.push(newItem);	
	}
	

}