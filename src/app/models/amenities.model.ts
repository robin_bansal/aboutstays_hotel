import {BaseModel} from '../models/base.models'
export class Amenities extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model=[
            {
                "name": "",
                "startTime": "",
                "endTime": "",
                "hotelId": "",
                "wingId": "",
                "floorId": "",
                "entryCriteria": "",
                "description": "",
                "mostPopular": [],
                "hasDeals": false,
                "provideReservations": false,
                "imageUrl": "",
                "additionalImageUrls": [],
                "reservationType": "",
                "primaryCuisine": "",
                "guestSenseCategory": "",
                "amenityType": "",
                "categoryId": "",
                "reservationCategoryName": "",
                "directionList": null
            }
        ]
		
	}

	getAmenityCat()
	{
		var restypes=Array();
		if(this.model.length<=0)
		{
			return [];
		}
		return this.model.filter(function(item: any){
			if(restypes.indexOf(item.reservationType)<0 && item.reservationType!=null && item.reservationType!="")
			{
				restypes.push(item.reservationType);
				return true;
			}else
			{
				return false;
			}
		})
	}

	getAmenityByCat(restype: any)
	{
		return this.model.filter(function(item: any){
			if(item.reservationType==restype)
			{
				return true;
			}else
			{
				return false;
			}
		})
	}

	update(data: any=null)
	{
		if(data!=null)
		{
			this.model.forEach(function(value: any, index: any, array: any){
				if(value.categoryId==data.categoryId)
				{
					for(var item in value){
						value[item]=data[item];
					}

					
					return false;
				}
			});
		}
	}
	

}
