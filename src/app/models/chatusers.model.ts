import {BaseModel} from './base.models';
export class ChatUsersModel extends BaseModel {
	public model: any;
	constructor() {
		super();
		this.model=[
            {
                "userData": {
                        "title": "",
                        "firstName": "",
                        "lastName": "",
                        "imageUrl": "",
                        "emailId": "",
                        "number": "",
                        "gender": "",
                        "userId": "",
                        "dob": "",
                        "anniversaryDate": "",
                        "maritalStatus": 2,
                        "homeAddress": {
                            "addressLine1": "",
                            "addressLine2": "",
                            "latitude": "",
                            "longitude": "",
                            "city": "",
                            "state": "",
                            "pincode": "",
                            "country": "",
                            "description": ""
                        },
                        "officeAddress": {
                            "addressLine1": "",
                            "addressLine2": "",
                            "latitude": "",
                            "longitude": "",
                            "city": "",
                            "state": "",
                            "pincode": "",
                            "country": "",
                            "description": ""
                        },
                        "listStayPreferences": [],
                        "listIdentityDocs": []
                    },
                    "staysData": {
                        "staysId": "",
                        "bookingId": "",
                        "hotelId": "",
                        "roomId": "",
                        "userId": "",
                        "checkInDate": "",
                        "checkInTime": "",
                        "checkOutTime": "",
                        "checkOutDate": "",
                        "official": false,
                        "staysOrdering": 0
                    },
                    "unreadMessageCount": 0
                }
            ]
	}


}