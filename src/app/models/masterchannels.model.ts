import {BaseModel} from '../models/base.models'
export class MasterChannelsModel extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model=[
            {
                "name": "",
                "logo": "",
                "imageUrl": "",
                "description": "",
                "channelId": "",
                "genereDetails": {
                    "name": "",
                    "logo": "",
                    "masterGenereId": ""
                }
            }
        ]
		
	}

	findById(channel_id: any): any
	{
		let temp=null;
		this.model.forEach(function(value: any){
			if(value.channelId==channel_id)
			{
				temp = value;
			}
		})

		return temp;
	}

	

		

}
