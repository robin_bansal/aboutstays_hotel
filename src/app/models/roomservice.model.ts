export class RoomServiceModel
{
	public service: any;
	public items: Array<any>;
	constructor()
	{
		this.service={
				"id": "",
		        "listMenuItem": [
		            {
	                    "type": 0,
	                    "typeName": "",
	                    "imageUrl": "",
	                    "timing": "",
	                    "startTime": "",
	                    "endTime": "",
	                    "allDay": false
	                }
	            ]
	        }
	    this.items=[
	    	{
	            "fooditemId": "",
	            "roomServicesId": "",
	            "hotelId": "",
	            "imageUrl": "",
	            "name": "",
	            "serves": 0,
	            "cuisineType": "",
	            "mrp": 0,
	            "price": 0,
	            "description": "",
	            "foodLabel": 0,
	            "spicyType": 0,
	            "recommended": false,
	            "type": 0,
	            "category": "",
	            "showOnApp": false,
	            "listGoesWellItems": []
	        }
	    ]
	}

	getCategory(type: any)
	{
		var category=Array();
		if(this.items[type]==undefined)
		{
			return [];
		}
		return this.items[type].filter(function(item: any){
			if(category.indexOf(item.category)<0 && item.category!=null && item.category!="")
			{
				category.push(item.category);
				return true;
			}else
			{
				return false;
			}
		})
	
	}

	deleteItem(type: any, itemId: any)
	{
		var temp=Array();
		this.items[type].forEach(function(value: any){
				if(value.fooditemId!=itemId)
				{
					temp.push(value);
				}
		})
		this.items[type]=temp;
	}

	addItem(item: any)
	{
		this.items[item.type].push(item);
	}

	getItemByTypeAndId(type: any, itemId: any)
	{
		let returns: any;
		this.items[type].forEach(function(value: any){
			if(value.fooditemId==itemId)
			{
				returns=value;
			}
		})
		return returns;	
	}

	getCategoryItems(type: any, category: any)
	{
		if(this.items[type]==undefined)
		{
			return [];
		}
		return this.items[type].filter(function(item: any){
			if(item.category==category && item.category!=null && item.category!="")
			{
				return true;
			}else
			{
				return false;
			}
		})	
	}

	updateItem(item: any)
	{
		let itemId=item.fooditemId;
		let type=item.type;
		let returns: any;
		this.items[type].forEach(function(value: any){
			if(value.fooditemId==itemId)
			{

				for(var attr in value)
				{
					value[attr]=item[attr];
				}
				return false;
			}
		})

	}


}