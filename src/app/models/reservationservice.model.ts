export class reservationServiceModel
{
	public service: any;
	public items: Array<any>;
	constructor()
	{
		this.service={
				"id": "",
		        "listPacks": [
		            
	            ]
	        }
	    this.items=[]
	}

	getCategory(type: any)
	{
		var category=Array();
		if(this.items[type]==undefined)
		{
			return [];
		}
		return this.items[type].filter(function(item: any){
			if(category.indexOf(item.category)<0 && item.category!=null && item.category!="")
			{
				category.push(item.category);
				return true;
			}else
			{
				return false;
			}
		})
	
	}

	getCategoryItems(type: any, category: any)
	{
		if(this.items[type]==undefined)
		{
			return [];
		}
		return this.items[type].filter(function(item: any){
			if(item.category==category && item.category!=null && item.category!="")
			{
				return true;
			}else
			{
				return false;
			}
		})	
	}
}