import {BaseModel} from '../models/base.models'
export class RoomCategory extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model=[
			{
	            "categoryName": "",
	            "categoryCode": "",
	            "count": 0,
	            "imageUrl": "",
	            "description": "",
	            "size": "",
	            "sizeMetric": "",
	            "adultCapacity": 0,
	            "childCapacity": 0,
	            "additionalImageUrls": [],
	            "earlyCheckinInfo": {},
	            "lateCheckoutInfo": {},
	            "hotelId": this.loginDetails.hotelId,
	            "roomCategoryId": "",
	            "inRoomAmenities": [],
	            "preferenceBasedAmenities": []
        	}]
		
	}


	roomCatUpdateByCatId(data: Object, catid: any)
	{
		var i=0;
		this.model.forEach(function(value: any, index: any){
			if(value.roomCategoryId==catid)
			{
				i=index;
			}
		})
		this.updateByIndex(data, i);
	}

	findRoomcatById(id: any)
	{
		let cat: any;
		this.model.forEach(function(value: any){
			if(value.roomCategoryId==id)
			{
				cat=value;
				return false;
			}
		})
		return cat;
	}
	
		

}




export class Wing extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model=[
			{
            "name": "",
            "code": "",
            "hotelId": this.loginDetails.hotelId,
            "wingId": "",
            "floorsCount": 0,
            "roomsCount": 0
        	}]
		
	}

	increaseCount(wid: any, fc: number, rc: number)
	{
		this.model.forEach(function(value: any, index: number){
			
			if(value.wingId==wid)
			{
				value.floorsCount=value.floorsCount+fc;
				value.roomsCount=value.roomsCount+rc;
			}

		}, this)
	}

	getById(id: any)
	{
		let returns: any;
		this.model.forEach(function(value: any, index: any){
			if(value.wingId==id)
			{
				returns=value;
				return false;
			}
		})
		return returns;
	}

}


export class Floor extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model=[
			{
            "name": "",
            "code": "",
            "hotelId": this.loginDetails.hotelId,
            "wingId": "",
            "floorId": "",
            "roomsCount": 0
        	}]
		
	}

	getByWingId(wid: any)
	{
		var returns=Array();
		this.model.forEach(function(value: any, index: any){
			if(value.wingId==wid)
			{
				returns.push(value);
			}
		})
		return returns;
	}
	getById(id: any)
	{
		let returns: any;
		this.model.forEach(function(value: any, index: any){
			if(value.floorId==id)
			{
				returns=value;
				return false;
			}
		})
		return returns;
	}

	increaseCount(fid: any, rc: number)
	{
		this.model.forEach(function(value: any, index: number){
			
			if(value.floorId==fid)
			{
				value.roomsCount=value.roomsCount+rc;
			}

		}, this)
	}

		

	
}

export class Room extends BaseModel
{
	public model: any;
	constructor()
	{
		super();
		this.model=[
			{
            "roomName": "",
            "roomNumber": "",
            "wingId": "",
            "floorId": "",
            "roomCategoryData": {
	            	"roomCategoryId": ""
	            },
            "hotelId": this.loginDetails.hotelId,
            "roomId": "",
            "additionalAmenities": []
        	}]
		
	}

	getByFloorId(fid: any)
	{
		var returns=Array();
		this.model.forEach(function(value: any, index: any){
			if(value.floorId==fid)
			{
				returns.push(value);
			}
		})
		return returns;
	}

	deleteRoomById(roomId: any)
	{
		this.model.forEach(function(value: any, index: number){
			if(value.roomId==roomId)
			{
				this.model.splice(index, 1);
				return false;
			}
		}, this)
	}

	getByWingId(wid: any)
	{
		var returns=Array();
		this.model.forEach(function(value: any, index: any){
			if(value.wingId==wid)
			{
				returns.push(value);
			}
		})
		return returns;
	}

	updateRoom(room: any)
	{
		this.model.forEach(function(value: any, index: number){
			if(value.roomId==room.roomId)
			{
				for(var attr in value)
				{
					value[attr]=room[attr];
				}
				
				return false;
			}
		}, this)	
	}
	
}