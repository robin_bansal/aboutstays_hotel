export class LaundryServiceModel
{
	public service: any;
	public items: any;
	constructor()
	{
		this.service={
				"serviceId": "",
		        "supportedLaundryTypeList": [{
                    "type": 0,
                    "typeName": "",
                    "imageUrl": "",
                    "sameDayAvailable": false
                }]
	        }
	    this.items=[
	    	{
	            "itemId": "",
	            "hotelId": "",
	            "laundryServicesId": "",
	            "name": "",
	            "laundryFor": 0,
	            "type": 0,
	            "mrp": 0,
	            "price": 0,
	            "category": "",
	            "sameDayPrice": 0,
	            "showOnApp": false
	        }
	    ]
	}

	getCategory(type: any)
	{
		var category=Array();
		if(this.items[type]==undefined)
		{
			return [];
		}
		return this.items[type].filter(function(item: any){
			if(category.indexOf(item.category)<0 && item.category!=null && item.category!="")
			{
				category.push(item.category);
				return true;
			}else
			{
				return false;
			}
		})
	
	}

	getCategoryItems(type: any, category: any)
	{
		if(this.items[type]==undefined)
		{
			return [];
		}
		return this.items[type].filter(function(item: any){
			if(item.category==category && item.category!=null && item.category!="")
			{
				return true;
			}else
			{
				return false;
			}
		})	
	}

	updateAllItem(upitems: any)
	{
		
		upitems.forEach(function(upitem: any){

			this.items[upitem.type].forEach(function(item:any){
				if(item.itemId==upitem.itemId)
				{
					item.name=upitem.name;
					item.showOnApp=upitem.showOnApp;
					item.price=upitem.price;
					item.sameDayPrice=upitem.sameDayPrice;
				}
			})
			
		}, this)
	}

	deleteItem(type: any, itemId: any)
	{
		var temp=Array();
		this.items[type].forEach(function(value: any){
				if(value.itemId!=itemId)
				{
					temp.push(value);
				}
		})
		this.items[type]=temp;
	}

	addItem(item: any)
	{
		this.items[item.type].push(item);
	}
}