import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

@Component({
  selector: 'my-app',
  templateUrl: './crew.html',
  styleUrls: ["css/header.css", "css/setting.css", "css/sidemenu.css"],
})

export class CrewComponent {}