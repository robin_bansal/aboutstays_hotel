import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrewComponent } from './crew.component';


export const routes: Routes = [
  { path: '',  component: CrewComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);