import { NgModule } from '@angular/core';
import {routing} from './crew.routes';
import { CrewComponent } from './crew.component';


@NgModule({
	imports: [routing],
	declarations: [CrewComponent],
	bootstrap: [CrewComponent]
})

export class CrewModule{  }