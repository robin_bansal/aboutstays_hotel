import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingAmenitiesComponent } from './setting.amenities.component';


export const routes: Routes = [
  { path: '',  component: SettingAmenitiesComponent }
];


export const routing: ModuleWithProviders=RouterModule.forChild(routes);