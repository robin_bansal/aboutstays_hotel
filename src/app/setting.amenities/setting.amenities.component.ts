import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import {Http, Request, RequestMethod, Response, Headers, RequestOptions} from '@angular/http';
import { Location }                 from '@angular/common';
import { AmenitiesService } from '../services/amenities.services';
import {Wing, Floor} from '../models/floormap.model';
import {Amenities} from '../models/amenities.model';
import {FloormapService} from '../services/floormap.services';
@Component({
  templateUrl: './setting.amenities.html',
  styleUrls: ["css/setting.css"],
  providers: [FloormapService]
})

export class SettingAmenitiesComponent extends AmenitiesService{
	public accord: Array<any>;
	public amenityEdit: Array<any>;
	public amenityFormData: any;
	public wings = new Wing;
	public floors = new Floor;
	public amenities=new Amenities;
	constructor(public _http: Http, private _floormapservice: FloormapService){
		super(_http);
		this.accord=Array();
		this.amenityEdit=Array();
	}

	ngOnInit(): void
	{
		this.startLoader();
			this.loadAmenities()
				.subscribe(
	            res => {res.error==true?this.throwError(res.message):this.amenities.loadModel(res.data.categories);},
	            error => this.throwError(error),
	            () => {}
	            );

	    this._floormapservice.loadWings()
          	.subscribe(
	            res => this.wings.loadModel(res.data),
	            error => this.throwError(error),
	            () => {}
	        );

	    this._floormapservice.loadFloors()
          	.subscribe(
	            res => {this.floors.loadModel(res.data); this.stopLoader()},
	            error => this.throwError(error),
	            () => {}
	        );
	}
	submitAmenityForm(valid: boolean)
	{
		if(valid)
			return false;
		this.startLoader();
		//console.log(JSON.stringify(this.amenityFormData));
		if(this.amenityFormData.categoryId=="")
		{
			this.addAmenities(this.amenityFormData)
				.subscribe(
		            res => {
		            	if(res.error){
		            		this.throwError(res.message);
		            	}else
		            	{
		            		this.amenities.update(this.amenityFormData); this.stopLoader();this.showAlert("success","Amenities Updated");
		            	}
		            },
		            error => this.throwError(error),
		            () => {}
	            );
		}else
		{
			this.updateAmenities(this.amenityFormData)
				.subscribe(
		            res => {
		            	if(res.error){
		            		this.throwError(res.message);
		            	}else
		            	{
		            		this.amenities.update(this.amenityFormData); this.stopLoader();this.showAlert("success","Amenities Updated");
		            	}
		            },
		            error => this.throwError(error),
		            () => {}
	            );	
		}

		return true;
		
	}

	addNewAmenityCategory(amenity: any, i:number)
	{
		var amenityModel=new Amenities;
		var temp=amenityModel.one();
		temp.reservationType=amenity.reservationType;
		temp.reservationCategoryName=amenity.reservationCategoryName;
		temp.imageUrl="http://media.gq.com/photos/573e23554bf860ff12c0e961/master/w_2000/best-restaurants-gq-0616-03.jpg";
		this.amenities.save(temp);
		var len=this.amenities.getAmenityByCat(amenity.reservationType).length;
		this.accord['sub' + i+'-'+(len-1)]=true;
		this.amenityEdit['a'+(len-1)]=true;

		this.amenityEditClick(temp);

	}
	cancelAmenityEdit()
	{
		if(this.amenityFormData.categoryId=="")
		{
			var len=this.amenities.model.length;
			this.amenities.delete(len-1);
		}
	}
	amenityEditClick(obj: any)
	{
		this.amenityFormData=JSON.parse(JSON.stringify(obj));
	}
	addMostPopular(mostpopular: any)
	{
		if(mostpopular.length<5)
		{
			mostpopular.push("Add Some Text Here...");
		}
	}
	removeMostPopular(mostpopular: any, i: number)
	{
		if(mostpopular.length>1)
		{
			mostpopular.splice(i,1);
		}	
	}
	removeAdditionImage(imageArray:any, i:number)
	{
		imageArray.splice(i,1);
	}
}