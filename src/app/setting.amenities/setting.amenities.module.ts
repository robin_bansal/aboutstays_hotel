import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { HttpModule } from '@angular/http';

import { SettingAmenitiesComponent } from './setting.amenities.component';
import { FormsModule }    from '@angular/forms';
import {routing} from './amenities.router';
@NgModule({
	imports: [CommonModule,HttpModule,FormsModule,routing],
	declarations: [SettingAmenitiesComponent],
	bootstrap: [SettingAmenitiesComponent]
})

export class SettingAmenitiesModule{  }